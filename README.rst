==================================
 RsLcx
==================================

.. image:: https://img.shields.io/pypi/v/RsLcx.svg
   :target: https://pypi.org/project/ RsLcx/

.. image:: https://readthedocs.org/projects/sphinx/badge/?version=master
   :target: https://RsLcx.readthedocs.io/

.. image:: https://img.shields.io/pypi/l/RsLcx.svg
   :target: https://pypi.python.org/pypi/RsLcx/

.. image:: https://img.shields.io/pypi/pyversions/pybadges.svg
   :target: https://img.shields.io/pypi/pyversions/pybadges.svg

.. image:: https://img.shields.io/pypi/dm/RsLcx.svg
   :target: https://pypi.python.org/pypi/RsLcx/

Rohde & Schwarz LCR Meter RsLcx instrument driver.

Basic Hello-World code:

.. code-block:: python

    from RsLcx import *

    instr = RsLcx('TCPIP::192.168.2.101::hislip0')
    idn = instr.query('*IDN?')
    print('Hello, I am: ' + idn)

Check out the full documentation on `ReadTheDocs <https://RsLcx.readthedocs.io/>`_.

Supported instruments: LCX

The package is hosted here: https://pypi.org/project/RsLcx/

Documentation: https://RsLcx.readthedocs.io/

Examples: https://github.com/Rohde-Schwarz/Examples/tree/main/Misc/Python/RsLcx_ScpiPackage


Version history
----------------

	Latest release notes summary: First release for FW 2.007

	Version 2.7
		- First release for FW 2.007

