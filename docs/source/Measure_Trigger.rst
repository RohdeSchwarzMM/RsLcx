Trigger
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: MEASure:TRIGger:DELay

.. code-block:: python

	MEASure:TRIGger:DELay



.. autoclass:: RsLcx.Implementations.Measure.Trigger.TriggerCls
	:members:
	:undoc-members:
	:noindex: