Range
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FUNCtion:IMPedance:RANGe:AUTO
	single: FUNCtion:IMPedance:RANGe:HOLD
	single: FUNCtion:IMPedance:RANGe:VALue

.. code-block:: python

	FUNCtion:IMPedance:RANGe:AUTO
	FUNCtion:IMPedance:RANGe:HOLD
	FUNCtion:IMPedance:RANGe:VALue



.. autoclass:: RsLcx.Implementations.Function.Impedance.Range.RangeCls
	:members:
	:undoc-members:
	:noindex: