Measure
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: MEASure:VOLTage
	single: MEASure:CURRent
	single: MEASure:MODE
	single: MEASure:ACCuracy

.. code-block:: python

	MEASure:VOLTage
	MEASure:CURRent
	MEASure:MODE
	MEASure:ACCuracy



.. autoclass:: RsLcx.Implementations.Measure.MeasureCls
	:members:
	:undoc-members:
	:noindex:



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Measure_Trigger.rst