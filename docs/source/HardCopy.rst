HardCopy
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: HCOPy:DATA

.. code-block:: python

	HCOPy:DATA



.. autoclass:: RsLcx.Implementations.HardCopy.HardCopyCls
	:members:
	:undoc-members:
	:noindex:



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	HardCopy_FormatPy.rst
	HardCopy_Size.rst