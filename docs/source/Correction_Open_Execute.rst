Execute
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CORRection:OPEN:EXECute

.. code-block:: python

	CORRection:OPEN:EXECute



.. autoclass:: RsLcx.Implementations.Correction.Open.Execute.ExecuteCls
	:members:
	:undoc-members:
	:noindex: