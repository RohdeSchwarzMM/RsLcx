Open
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CORRection:OPEN:STATe
	single: CORRection:OPEN:MODE

.. code-block:: python

	CORRection:OPEN:STATe
	CORRection:OPEN:MODE



.. autoclass:: RsLcx.Implementations.Correction.Open.OpenCls
	:members:
	:undoc-members:
	:noindex:



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Correction_Open_Execute.rst