Local
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SYSTem:LOCal

.. code-block:: python

	SYSTem:LOCal



.. autoclass:: RsLcx.Implementations.System.Local.LocalCls
	:members:
	:undoc-members:
	:noindex: