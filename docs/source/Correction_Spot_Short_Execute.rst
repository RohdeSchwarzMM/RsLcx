Execute
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CORRection:SPOT<Spot>:SHORt:EXECute

.. code-block:: python

	CORRection:SPOT<Spot>:SHORt:EXECute



.. autoclass:: RsLcx.Implementations.Correction.Spot.Short.Execute.ExecuteCls
	:members:
	:undoc-members:
	:noindex: