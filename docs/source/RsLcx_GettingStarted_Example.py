"""Getting started - how to work with RsLcx Python package.
This example performs basic RF settings on an R&S LCX instrument.
It shows the RsLcx calls and their corresponding SCPI commands.
Notice that the python RsLcx interfaces track the SCPI commands syntax."""

from RsLcx import *

# Open the session
lcx = RsLcx('TCPIP::10.102.52.44::HISLIP', False, False)
# Greetings, stranger...
print(f'Hello, I am: {lcx.utilities.idn_string}')

#   SOURce:FREQuency:FIXed 223000000
lcx.source.frequency.cw.set_value(223E6)

lcx.source.areGenerator.radar.base.set_attenuation(10)

# Close the session
lcx.close()
