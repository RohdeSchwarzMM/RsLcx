Open
----------------------------------------





.. autoclass:: RsLcx.Implementations.Correction.Spot.Open.OpenCls
	:members:
	:undoc-members:
	:noindex:



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Correction_Spot_Open_Execute.rst