Size
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: HCOPy:SIZE:X
	single: HCOPy:SIZE:Y

.. code-block:: python

	HCOPy:SIZE:X
	HCOPy:SIZE:Y



.. autoclass:: RsLcx.Implementations.HardCopy.Size.SizeCls
	:members:
	:undoc-members:
	:noindex: