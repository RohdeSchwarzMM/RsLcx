Spot<Spot>
----------------------------------------

.. rubric:: RepCap Settings

.. code-block:: python

	# Range: Nr1 .. Nr32
	rc = driver.correction.spot.repcap_spot_get()
	driver.correction.spot.repcap_spot_set(repcap.Spot.Nr1)





.. autoclass:: RsLcx.Implementations.Correction.Spot.SpotCls
	:members:
	:undoc-members:
	:noindex:



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Correction_Spot_Load.rst
	Correction_Spot_Open.rst
	Correction_Spot_Short.rst