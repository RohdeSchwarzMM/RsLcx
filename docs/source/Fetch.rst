Fetch
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:IMPedance
	single: FETCh

.. code-block:: python

	FETCh:IMPedance
	FETCh



.. autoclass:: RsLcx.Implementations.Fetch.FetchCls
	:members:
	:undoc-members:
	:noindex: