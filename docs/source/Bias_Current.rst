Current
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: BIAS:CURRent:LEVel

.. code-block:: python

	BIAS:CURRent:LEVel



.. autoclass:: RsLcx.Implementations.Bias.Current.CurrentCls
	:members:
	:undoc-members:
	:noindex: