Log
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: LOG:STATe
	single: LOG:MODE
	single: LOG:FNAMe
	single: LOG:STIMe

.. code-block:: python

	LOG:STATe
	LOG:MODE
	LOG:FNAMe
	LOG:STIMe



.. autoclass:: RsLcx.Implementations.Log.LogCls
	:members:
	:undoc-members:
	:noindex:



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Log_Count.rst
	Log_Duration.rst
	Log_Interval.rst