Duration
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: LOG:DURation

.. code-block:: python

	LOG:DURation



.. autoclass:: RsLcx.Implementations.Log.Duration.DurationCls
	:members:
	:undoc-members:
	:noindex: