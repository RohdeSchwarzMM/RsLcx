Usb
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: INTerfaces:USB:CLASs

.. code-block:: python

	INTerfaces:USB:CLASs



.. autoclass:: RsLcx.Implementations.Interfaces.Usb.UsbCls
	:members:
	:undoc-members:
	:noindex: