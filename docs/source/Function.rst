Function
----------------------------------------





.. autoclass:: RsLcx.Implementations.Function.FunctionCls
	:members:
	:undoc-members:
	:noindex:



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Function_Impedance.rst
	Function_Measurement.rst
	Function_Transformer.rst