Range
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FUNCtion:TRANsformer:RANGe:TYPE

.. code-block:: python

	FUNCtion:TRANsformer:RANGe:TYPE



.. autoclass:: RsLcx.Implementations.Function.Transformer.Range.RangeCls
	:members:
	:undoc-members:
	:noindex: