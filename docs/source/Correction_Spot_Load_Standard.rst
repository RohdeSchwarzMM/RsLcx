Standard
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CORRection:SPOT<Spot>:LOAD:STANdard

.. code-block:: python

	CORRection:SPOT<Spot>:LOAD:STANdard



.. autoclass:: RsLcx.Implementations.Correction.Spot.Load.Standard.StandardCls
	:members:
	:undoc-members:
	:noindex: