Measurement
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FUNCtion:MEASurement:TYPE

.. code-block:: python

	FUNCtion:MEASurement:TYPE



.. autoclass:: RsLcx.Implementations.Function.Measurement.MeasurementCls
	:members:
	:undoc-members:
	:noindex: