Setting
----------------------------------------





.. autoclass:: RsLcx.Implementations.System.Setting.SettingCls
	:members:
	:undoc-members:
	:noindex:



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	System_Setting_Default.rst