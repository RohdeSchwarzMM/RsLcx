Short
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CORRection:SHORt:STATe
	single: CORRection:SHORt:MODE

.. code-block:: python

	CORRection:SHORt:STATe
	CORRection:SHORt:MODE



.. autoclass:: RsLcx.Implementations.Correction.Short.ShortCls
	:members:
	:undoc-members:
	:noindex:



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Correction_Short_Execute.rst