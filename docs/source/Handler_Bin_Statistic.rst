Statistic
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: HANDler:BIN:STATistic:COUNt
	single: HANDler:BIN:STATistic:RESet
	single: HANDler:BIN:STATistic

.. code-block:: python

	HANDler:BIN:STATistic:COUNt
	HANDler:BIN:STATistic:RESet
	HANDler:BIN:STATistic



.. autoclass:: RsLcx.Implementations.Handler.Bin.Statistic.StatisticCls
	:members:
	:undoc-members:
	:noindex: