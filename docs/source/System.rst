System
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SYSTem:UPTime

.. code-block:: python

	SYSTem:UPTime



.. autoclass:: RsLcx.Implementations.System.SystemCls
	:members:
	:undoc-members:
	:noindex:



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	System_Beeper.rst
	System_Communicate.rst
	System_Date.rst
	System_Hw.rst
	System_Key.rst
	System_Local.rst
	System_Remote.rst
	System_Restart.rst
	System_RwLock.rst
	System_Setting.rst
	System_Time.rst