Frequency
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FREQuency:CW

.. code-block:: python

	FREQuency:CW



.. autoclass:: RsLcx.Implementations.Frequency.FrequencyCls
	:members:
	:undoc-members:
	:noindex: