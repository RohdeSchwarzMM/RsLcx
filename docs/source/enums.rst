Enums
=========

HcopyFormat
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.HcopyFormat.BMP
	# All values (2x):
	BMP | PNG

Impedance
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.Impedance.HIGH
	# All values (4x):
	HIGH | LOW | R10 | R100

ImpedanceType
----------------------------------------------------

.. code-block:: python

	# First value:
	value = enums.ImpedanceType.CPD
	# Last value:
	value = enums.ImpedanceType.ZTR
	# All values (24x):
	CPD | CPG | CPQ | CPRP | CSD | CSQ | CSRS | GB
	LPD | LPG | LPQ | LPRP | LSD | LSQ | LSRS | MTD
	NTD | RDC | RPB | RX | YTD | YTR | ZTD | ZTR

IntervalParameter
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.IntervalParameter.POINts
	# All values (2x):
	POINts | STEPsize

LoggingMode
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.LoggingMode.COUNt
	# All values (4x):
	COUNt | DURation | SPAN | UNLimited

MeasurementMode
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.MeasurementMode.CONTinuous
	# All values (2x):
	CONTinuous | TRIGgered

MeasurementTimeMode
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.MeasurementTimeMode.DEFault
	# All values (4x):
	DEFault | LONG | MEDium | SHORt

MeasurementType
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.MeasurementType.C
	# All values (4x):
	C | L | R | T

MinOrMax
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.MinOrMax.MAX
	# All values (4x):
	MAX | MAXimum | MIN | MINimum

SweepParameter
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.SweepParameter.FREQuency
	# All values (4x):
	FREQuency | IBIas | VBIas | VOLTage

TurnRatio
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.TurnRatio.N50
	# All values (2x):
	N50 | N500

UsbClass
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.UsbClass.CDC
	# All values (2x):
	CDC | TMC

