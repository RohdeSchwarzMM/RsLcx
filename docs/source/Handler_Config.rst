Config
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: HANDler:CONFig:PATH

.. code-block:: python

	HANDler:CONFig:PATH



.. autoclass:: RsLcx.Implementations.Handler.Config.ConfigCls
	:members:
	:undoc-members:
	:noindex: