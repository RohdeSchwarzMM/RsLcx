Load
----------------------------------------





.. autoclass:: RsLcx.Implementations.Correction.Spot.Load.LoadCls
	:members:
	:undoc-members:
	:noindex:



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Correction_Spot_Load_Execute.rst
	Correction_Spot_Load_Standard.rst