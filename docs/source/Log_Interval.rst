Interval
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: LOG:INTerval

.. code-block:: python

	LOG:INTerval



.. autoclass:: RsLcx.Implementations.Log.Interval.IntervalCls
	:members:
	:undoc-members:
	:noindex: