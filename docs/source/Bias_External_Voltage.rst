Voltage
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: BIAS:EXTernal:VOLTage:STATe

.. code-block:: python

	BIAS:EXTernal:VOLTage:STATe



.. autoclass:: RsLcx.Implementations.Bias.External.Voltage.VoltageCls
	:members:
	:undoc-members:
	:noindex: