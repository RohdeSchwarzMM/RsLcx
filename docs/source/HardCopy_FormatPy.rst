FormatPy
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: HCOPy:FORMat

.. code-block:: python

	HCOPy:FORMat



.. autoclass:: RsLcx.Implementations.HardCopy.FormatPy.FormatPyCls
	:members:
	:undoc-members:
	:noindex: