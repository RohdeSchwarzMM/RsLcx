Read
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: READ:IMPedance
	single: READ

.. code-block:: python

	READ:IMPedance
	READ



.. autoclass:: RsLcx.Implementations.Read.ReadCls
	:members:
	:undoc-members:
	:noindex: