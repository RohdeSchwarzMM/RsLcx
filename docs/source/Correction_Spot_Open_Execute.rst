Execute
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CORRection:SPOT<Spot>:OPEN:EXECute

.. code-block:: python

	CORRection:SPOT<Spot>:OPEN:EXECute



.. autoclass:: RsLcx.Implementations.Correction.Spot.Open.Execute.ExecuteCls
	:members:
	:undoc-members:
	:noindex: