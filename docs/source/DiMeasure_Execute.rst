Execute
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: DIMeasure:EXECute

.. code-block:: python

	DIMeasure:EXECute



.. autoclass:: RsLcx.Implementations.DiMeasure.Execute.ExecuteCls
	:members:
	:undoc-members:
	:noindex: