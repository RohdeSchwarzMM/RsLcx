Lan
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SYSTem:COMMunicate:LAN:DHCP
	single: SYSTem:COMMunicate:LAN:ADDRess
	single: SYSTem:COMMunicate:LAN:SMASk
	single: SYSTem:COMMunicate:LAN:DGATeway
	single: SYSTem:COMMunicate:LAN:HOSTname
	single: SYSTem:COMMunicate:LAN:MAC
	single: SYSTem:COMMunicate:LAN:RESet
	single: SYSTem:COMMunicate:LAN:EDITed

.. code-block:: python

	SYSTem:COMMunicate:LAN:DHCP
	SYSTem:COMMunicate:LAN:ADDRess
	SYSTem:COMMunicate:LAN:SMASk
	SYSTem:COMMunicate:LAN:DGATeway
	SYSTem:COMMunicate:LAN:HOSTname
	SYSTem:COMMunicate:LAN:MAC
	SYSTem:COMMunicate:LAN:RESet
	SYSTem:COMMunicate:LAN:EDITed



.. autoclass:: RsLcx.Implementations.System.Communicate.Lan.LanCls
	:members:
	:undoc-members:
	:noindex:



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	System_Communicate_Lan_Apply.rst
	System_Communicate_Lan_Discard.rst