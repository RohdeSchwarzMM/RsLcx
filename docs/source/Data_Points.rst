Points
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: DATA:POINts

.. code-block:: python

	DATA:POINts



.. autoclass:: RsLcx.Implementations.Data.Points.PointsCls
	:members:
	:undoc-members:
	:noindex: