Correction
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CORRection:LENGth

.. code-block:: python

	CORRection:LENGth



.. autoclass:: RsLcx.Implementations.Correction.CorrectionCls
	:members:
	:undoc-members:
	:noindex:



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Correction_Load.rst
	Correction_Open.rst
	Correction_Short.rst
	Correction_Spot.rst