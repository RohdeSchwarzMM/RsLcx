Hw
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SYSTem:HW:VERSion

.. code-block:: python

	SYSTem:HW:VERSion



.. autoclass:: RsLcx.Implementations.System.Hw.HwCls
	:members:
	:undoc-members:
	:noindex: