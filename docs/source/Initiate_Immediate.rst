Immediate
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: INITiate:IMMediate

.. code-block:: python

	INITiate:IMMediate



.. autoclass:: RsLcx.Implementations.Initiate.Immediate.ImmediateCls
	:members:
	:undoc-members:
	:noindex: