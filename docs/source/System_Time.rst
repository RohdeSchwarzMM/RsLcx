Time
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SYSTem:TIME

.. code-block:: python

	SYSTem:TIME



.. autoclass:: RsLcx.Implementations.System.Time.TimeCls
	:members:
	:undoc-members:
	:noindex: