Text
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: DISPlay:WINDow:TEXT:DATA
	single: DISPlay:WINDow:TEXT:CLEar

.. code-block:: python

	DISPlay:WINDow:TEXT:DATA
	DISPlay:WINDow:TEXT:CLEar



.. autoclass:: RsLcx.Implementations.Display.Window.Text.TextCls
	:members:
	:undoc-members:
	:noindex: