Vnc
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SYSTem:COMMunicate:NETWork:VNC:STATe
	single: SYSTem:COMMunicate:NETWork:VNC:PORT

.. code-block:: python

	SYSTem:COMMunicate:NETWork:VNC:STATe
	SYSTem:COMMunicate:NETWork:VNC:PORT



.. autoclass:: RsLcx.Implementations.System.Communicate.Network.Vnc.VncCls
	:members:
	:undoc-members:
	:noindex: