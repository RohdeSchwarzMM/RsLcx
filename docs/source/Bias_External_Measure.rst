Measure
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: BIAS:EXTernal:MEASure:VOLTage

.. code-block:: python

	BIAS:EXTernal:MEASure:VOLTage



.. autoclass:: RsLcx.Implementations.Bias.External.Measure.MeasureCls
	:members:
	:undoc-members:
	:noindex: