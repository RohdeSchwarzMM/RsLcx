Bias
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: BIAS:STATe

.. code-block:: python

	BIAS:STATe



.. autoclass:: RsLcx.Implementations.Bias.BiasCls
	:members:
	:undoc-members:
	:noindex:



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Bias_Current.rst
	Bias_External.rst
	Bias_Voltage.rst