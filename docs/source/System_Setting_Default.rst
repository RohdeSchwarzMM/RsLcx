Default
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SYSTem:SETTing:DEFault:SAVE

.. code-block:: python

	SYSTem:SETTing:DEFault:SAVE



.. autoclass:: RsLcx.Implementations.System.Setting.Default.DefaultCls
	:members:
	:undoc-members:
	:noindex: