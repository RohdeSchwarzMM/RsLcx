Short
----------------------------------------





.. autoclass:: RsLcx.Implementations.Correction.Spot.Short.ShortCls
	:members:
	:undoc-members:
	:noindex:



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Correction_Spot_Short_Execute.rst