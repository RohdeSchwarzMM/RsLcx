Immediate
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SYSTem:BEEPer:COMPlete:IMMediate

.. code-block:: python

	SYSTem:BEEPer:COMPlete:IMMediate



.. autoclass:: RsLcx.Implementations.System.Beeper.Complete.Immediate.ImmediateCls
	:members:
	:undoc-members:
	:noindex: