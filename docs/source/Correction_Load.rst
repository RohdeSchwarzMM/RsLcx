Load
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CORRection:LOAD:STATe
	single: CORRection:LOAD:MODE

.. code-block:: python

	CORRection:LOAD:STATe
	CORRection:LOAD:MODE



.. autoclass:: RsLcx.Implementations.Correction.Load.LoadCls
	:members:
	:undoc-members:
	:noindex: