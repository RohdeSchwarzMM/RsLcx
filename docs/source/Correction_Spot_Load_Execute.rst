Execute
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CORRection:SPOT<Spot>:LOAD:EXECute

.. code-block:: python

	CORRection:SPOT<Spot>:LOAD:EXECute



.. autoclass:: RsLcx.Implementations.Correction.Spot.Load.Execute.ExecuteCls
	:members:
	:undoc-members:
	:noindex: