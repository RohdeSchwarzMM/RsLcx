Interval
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: DIMeasure:INTerval:TYPE
	single: DIMeasure:INTerval:STEPsize
	single: DIMeasure:INTerval:POINts

.. code-block:: python

	DIMeasure:INTerval:TYPE
	DIMeasure:INTerval:STEPsize
	DIMeasure:INTerval:POINts



.. autoclass:: RsLcx.Implementations.DiMeasure.Interval.IntervalCls
	:members:
	:undoc-members:
	:noindex: