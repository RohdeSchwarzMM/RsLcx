Immediate
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SYSTem:BEEPer:WARNing:IMMediate

.. code-block:: python

	SYSTem:BEEPer:WARNing:IMMediate



.. autoclass:: RsLcx.Implementations.System.Beeper.WarningPy.Immediate.ImmediateCls
	:members:
	:undoc-members:
	:noindex: