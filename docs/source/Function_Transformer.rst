Transformer
----------------------------------------





.. autoclass:: RsLcx.Implementations.Function.Transformer.TransformerCls
	:members:
	:undoc-members:
	:noindex:



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Function_Transformer_Range.rst