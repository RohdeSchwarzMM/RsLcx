Current
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CURRent:LEVel

.. code-block:: python

	CURRent:LEVel



.. autoclass:: RsLcx.Implementations.Current.CurrentCls
	:members:
	:undoc-members:
	:noindex: