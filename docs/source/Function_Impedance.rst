Impedance
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FUNCtion:IMPedance:TYPE
	single: FUNCtion:IMPedance:SOURce

.. code-block:: python

	FUNCtion:IMPedance:TYPE
	FUNCtion:IMPedance:SOURce



.. autoclass:: RsLcx.Implementations.Function.Impedance.ImpedanceCls
	:members:
	:undoc-members:
	:noindex:



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Function_Impedance_Range.rst