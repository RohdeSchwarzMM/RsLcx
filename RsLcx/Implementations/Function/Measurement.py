from ...Internal.Core import Core
from ...Internal.CommandsGroup import CommandsGroup
from ...Internal import Conversions
from ... import enums


# noinspection PyPep8Naming,PyAttributeOutsideInit,SpellCheckingInspection
class MeasurementCls:
	"""Measurement commands group definition. 1 total commands, 0 Subgroups, 1 group commands"""

	def __init__(self, core: Core, parent):
		self._core = core
		self._cmd_group = CommandsGroup("measurement", core, parent)

	# noinspection PyTypeChecker
	def get_type_py(self) -> enums.MeasurementType:
		"""SCPI: FUNCtion:MEASurement:TYPE \n
		Snippet: value: enums.MeasurementType = driver.function.measurement.get_type_py() \n
		Selects the measurement function. \n
			:return: measurement_type:
				- L: Impedance measurement.
				- C: Capacitance measurement.
				- R: Resistance measurement.
				- T: Transformer measurement."""
		response = self._core.io.query_str_with_opc('FUNCtion:MEASurement:TYPE?')
		return Conversions.str_to_scalar_enum(response, enums.MeasurementType)

	def set_type_py(self, measurement_type: enums.MeasurementType) -> None:
		"""SCPI: FUNCtion:MEASurement:TYPE \n
		Snippet: driver.function.measurement.set_type_py(measurement_type = enums.MeasurementType.C) \n
		Selects the measurement function. \n
			:param measurement_type:
				- L: Impedance measurement.
				- C: Capacitance measurement.
				- R: Resistance measurement.
				- T: Transformer measurement."""
		param = Conversions.enum_scalar_to_str(measurement_type, enums.MeasurementType)
		self._core.io.write_with_opc(f'FUNCtion:MEASurement:TYPE {param}')
