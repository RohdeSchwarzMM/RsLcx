from ....Internal.Core import Core
from ....Internal.CommandsGroup import CommandsGroup
from ....Internal import Conversions
from .... import enums


# noinspection PyPep8Naming,PyAttributeOutsideInit,SpellCheckingInspection
class ImpedanceCls:
	"""Impedance commands group definition. 5 total commands, 1 Subgroups, 2 group commands"""

	def __init__(self, core: Core, parent):
		self._core = core
		self._cmd_group = CommandsGroup("impedance", core, parent)

	@property
	def range(self):
		"""range commands group. 0 Sub-classes, 3 commands."""
		if not hasattr(self, '_range'):
			from .Range import RangeCls
			self._range = RangeCls(self._core, self._cmd_group)
		return self._range

	# noinspection PyTypeChecker
	def get_type_py(self) -> enums.ImpedanceType:
		"""SCPI: FUNCtion:IMPedance[:TYPE] \n
		Snippet: value: enums.ImpedanceType = driver.function.impedance.get_type_py() \n
		Selects the impedance parameter for the measurement corresponding to the measurement type, see method RsLcx.Function.
		Measurement.typePy. \n
			:return: impedance_type:
				- CPD | CPQ | CPG | CPRP | CSD | CSQ | CSRS: Capacitive measurement type: Cp (parallel capacitance) , Cs (serial capacitance) , D (dissipation factor) , Q (quality factor) , G (conductance) , Rp (parallel resistance) , Rs (serial resistance)
				- LPD | LPQ | LPG | LPRP | LPRDc | LSD | LSQ | LSRS | LSRDc: Inductive measurement type: Lp (parallel inductance) , Ls (serial inductance) , D (dissipation factor) , Q (quality factor) , G (conductance) , Rp (parallel resistance) , Rs (serial resistance) , RDc (direct current resistance)
				- RX | RPB | RDC | MTD | NTD | ZTD | ZTR | GB | YTD | YTR: Resistance measurement type: R (resistance) , X impedance, Rp (parallel resistance) , RDC (direct current resistance) , B (susceptance) , M (mutual inductance) , N (transformer ratio) , Z (impedance) , G (conductance) , Y (admittance) , TD (phase angle degree) , TR (phase angle rad) """
		response = self._core.io.query_str('FUNCtion:IMPedance:TYPE?')
		return Conversions.str_to_scalar_enum(response, enums.ImpedanceType)

	def set_type_py(self, impedance_type: enums.ImpedanceType) -> None:
		"""SCPI: FUNCtion:IMPedance[:TYPE] \n
		Snippet: driver.function.impedance.set_type_py(impedance_type = enums.ImpedanceType.CPD) \n
		Selects the impedance parameter for the measurement corresponding to the measurement type, see method RsLcx.Function.
		Measurement.typePy. \n
			:param impedance_type:
				- CPD | CPQ | CPG | CPRP | CSD | CSQ | CSRS: Capacitive measurement type: Cp (parallel capacitance) , Cs (serial capacitance) , D (dissipation factor) , Q (quality factor) , G (conductance) , Rp (parallel resistance) , Rs (serial resistance)
				- LPD | LPQ | LPG | LPRP | LPRDc | LSD | LSQ | LSRS | LSRDc: Inductive measurement type: Lp (parallel inductance) , Ls (serial inductance) , D (dissipation factor) , Q (quality factor) , G (conductance) , Rp (parallel resistance) , Rs (serial resistance) , RDc (direct current resistance)
				- RX | RPB | RDC | MTD | NTD | ZTD | ZTR | GB | YTD | YTR: Resistance measurement type: R (resistance) , X impedance, Rp (parallel resistance) , RDC (direct current resistance) , B (susceptance) , M (mutual inductance) , N (transformer ratio) , Z (impedance) , G (conductance) , Y (admittance) , TD (phase angle degree) , TR (phase angle rad) """
		param = Conversions.enum_scalar_to_str(impedance_type, enums.ImpedanceType)
		self._core.io.write(f'FUNCtion:IMPedance:TYPE {param}')

	# noinspection PyTypeChecker
	def get_source(self) -> enums.Impedance:
		"""SCPI: FUNCtion:IMPedance:SOURce \n
		Snippet: value: enums.Impedance = driver.function.impedance.get_source() \n
		Selects the output impedance for the measurement. \n
			:return: impedance:
				- LOW | R10: Sets 10 Ω output impedance.
				- HIGH | R100: Sets 100 Ω output impedance."""
		response = self._core.io.query_str('FUNCtion:IMPedance:SOURce?')
		return Conversions.str_to_scalar_enum(response, enums.Impedance)

	def set_source(self, impedance: enums.Impedance) -> None:
		"""SCPI: FUNCtion:IMPedance:SOURce \n
		Snippet: driver.function.impedance.set_source(impedance = enums.Impedance.HIGH) \n
		Selects the output impedance for the measurement. \n
			:param impedance:
				- LOW | R10: Sets 10 Ω output impedance.
				- HIGH | R100: Sets 100 Ω output impedance."""
		param = Conversions.enum_scalar_to_str(impedance, enums.Impedance)
		self._core.io.write(f'FUNCtion:IMPedance:SOURce {param}')
