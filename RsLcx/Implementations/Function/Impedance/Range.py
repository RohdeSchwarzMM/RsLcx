from ....Internal.Core import Core
from ....Internal.CommandsGroup import CommandsGroup
from ....Internal import Conversions


# noinspection PyPep8Naming,PyAttributeOutsideInit,SpellCheckingInspection
class RangeCls:
	"""Range commands group definition. 3 total commands, 0 Subgroups, 3 group commands"""

	def __init__(self, core: Core, parent):
		self._core = core
		self._cmd_group = CommandsGroup("range", core, parent)

	def get_auto(self) -> bool:
		"""SCPI: FUNCtion:IMPedance:RANGe:AUTO \n
		Snippet: value: bool = driver.function.impedance.range.get_auto() \n
		Activates automatic impedance range selection. To set the impedance range manually, use command method RsLcx.Function.
		Impedance.Range.value. \n
			:return: auto_range: No help available
		"""
		response = self._core.io.query_str('FUNCtion:IMPedance:RANGe:AUTO?')
		return Conversions.str_to_bool(response)

	def set_auto(self, auto_range: bool) -> None:
		"""SCPI: FUNCtion:IMPedance:RANGe:AUTO \n
		Snippet: driver.function.impedance.range.set_auto(auto_range = False) \n
		Activates automatic impedance range selection. To set the impedance range manually, use command method RsLcx.Function.
		Impedance.Range.value. \n
			:param auto_range: No help available
		"""
		param = Conversions.bool_to_str(auto_range)
		self._core.io.write(f'FUNCtion:IMPedance:RANGe:AUTO {param}')

	def get_hold(self) -> bool:
		"""SCPI: FUNCtion:IMPedance:RANGe:HOLD \n
		Snippet: value: bool = driver.function.impedance.range.get_hold() \n
		Freezes the set impedance measurement range. \n
			:return: locks_selected_range: No help available
		"""
		response = self._core.io.query_str('FUNCtion:IMPedance:RANGe:HOLD?')
		return Conversions.str_to_bool(response)

	def set_hold(self, locks_selected_range: bool) -> None:
		"""SCPI: FUNCtion:IMPedance:RANGe:HOLD \n
		Snippet: driver.function.impedance.range.set_hold(locks_selected_range = False) \n
		Freezes the set impedance measurement range. \n
			:param locks_selected_range: No help available
		"""
		param = Conversions.bool_to_str(locks_selected_range)
		self._core.io.write(f'FUNCtion:IMPedance:RANGe:HOLD {param}')

	def get_value(self) -> float:
		"""SCPI: FUNCtion:IMPedance:RANGe[:VALue] \n
		Snippet: value: float = driver.function.impedance.range.get_value() \n
		Sets the impedance range value. For setting the parameter manually, disable auto selection with method RsLcx.Function.
		Impedance.Range.auto. \n
			:return: range_py: No help available
		"""
		response = self._core.io.query_str('FUNCtion:IMPedance:RANGe:VALue?')
		return Conversions.str_to_float(response)

	def set_value(self, range_py: float) -> None:
		"""SCPI: FUNCtion:IMPedance:RANGe[:VALue] \n
		Snippet: driver.function.impedance.range.set_value(range_py = 1.0) \n
		Sets the impedance range value. For setting the parameter manually, disable auto selection with method RsLcx.Function.
		Impedance.Range.auto. \n
			:param range_py: No help available
		"""
		param = Conversions.decimal_value_to_str(range_py)
		self._core.io.write(f'FUNCtion:IMPedance:RANGe:VALue {param}')
