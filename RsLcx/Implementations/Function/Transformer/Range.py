from ....Internal.Core import Core
from ....Internal.CommandsGroup import CommandsGroup
from ....Internal import Conversions
from .... import enums


# noinspection PyPep8Naming,PyAttributeOutsideInit,SpellCheckingInspection
class RangeCls:
	"""Range commands group definition. 1 total commands, 0 Subgroups, 1 group commands"""

	def __init__(self, core: Core, parent):
		self._core = core
		self._cmd_group = CommandsGroup("range", core, parent)

	# noinspection PyTypeChecker
	def get_type_py(self) -> enums.TurnRatio:
		"""SCPI: FUNCtion:TRANsformer:RANGe[:TYPE] \n
		Snippet: value: enums.TurnRatio = driver.function.transformer.range.get_type_py() \n
		Selects the impedance range for transformer measurement. \n
			:return: turn_ratio: No help available
		"""
		response = self._core.io.query_str('FUNCtion:TRANsformer:RANGe:TYPE?')
		return Conversions.str_to_scalar_enum(response, enums.TurnRatio)

	def set_type_py(self, turn_ratio: enums.TurnRatio) -> None:
		"""SCPI: FUNCtion:TRANsformer:RANGe[:TYPE] \n
		Snippet: driver.function.transformer.range.set_type_py(turn_ratio = enums.TurnRatio.N50) \n
		Selects the impedance range for transformer measurement. \n
			:param turn_ratio: No help available
		"""
		param = Conversions.enum_scalar_to_str(turn_ratio, enums.TurnRatio)
		self._core.io.write(f'FUNCtion:TRANsformer:RANGe:TYPE {param}')
