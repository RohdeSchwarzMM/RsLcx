from ...Internal.Core import Core
from ...Internal.CommandsGroup import CommandsGroup
from ...Internal import Conversions
from ...Internal.StructBase import StructBase
from ...Internal.ArgStruct import ArgStruct
from ... import enums


# noinspection PyPep8Naming,PyAttributeOutsideInit,SpellCheckingInspection
class MeasureCls:
	"""Measure commands group definition. 5 total commands, 1 Subgroups, 4 group commands"""

	def __init__(self, core: Core, parent):
		self._core = core
		self._cmd_group = CommandsGroup("measure", core, parent)

	@property
	def trigger(self):
		"""trigger commands group. 0 Sub-classes, 1 commands."""
		if not hasattr(self, '_trigger'):
			from .Trigger import TriggerCls
			self._trigger = TriggerCls(self._core, self._cmd_group)
		return self._trigger

	def get_voltage(self) -> float:
		"""SCPI: MEASure:VOLTage \n
		Snippet: value: float = driver.measure.get_voltage() \n
		Queries the voltage value following next in the measurement. \n
			:return: voltage: No help available
		"""
		response = self._core.io.query_str('MEASure:VOLTage?')
		return Conversions.str_to_float(response)

	def get_current(self) -> float:
		"""SCPI: MEASure:CURRent \n
		Snippet: value: float = driver.measure.get_current() \n
		Queries the current value following next in the measurement. \n
			:return: current: No help available
		"""
		response = self._core.io.query_str('MEASure:CURRent?')
		return Conversions.str_to_float(response)

	# noinspection PyTypeChecker
	def get_mode(self) -> enums.MeasurementMode:
		"""SCPI: MEASure:MODE \n
		Snippet: value: enums.MeasurementMode = driver.measure.get_mode() \n
		Selects whether the R&S LCX starts and continues a measurement, or starts on initiated trigger events. \n
			:return: measurement_mode:
				- CONTinuous: Restarts the measurement automatically after a measurement cycle has been completed.
				- TRIGgered: Starts a measurement cycle initiated by a trigger signal.To delay the measurement start to a certain extent, use command MEASure:TRIGger:DELay."""
		response = self._core.io.query_str('MEASure:MODE?')
		return Conversions.str_to_scalar_enum(response, enums.MeasurementMode)

	def set_mode(self, measurement_mode: enums.MeasurementMode) -> None:
		"""SCPI: MEASure:MODE \n
		Snippet: driver.measure.set_mode(measurement_mode = enums.MeasurementMode.CONTinuous) \n
		Selects whether the R&S LCX starts and continues a measurement, or starts on initiated trigger events. \n
			:param measurement_mode:
				- CONTinuous: Restarts the measurement automatically after a measurement cycle has been completed.
				- TRIGgered: Starts a measurement cycle initiated by a trigger signal.To delay the measurement start to a certain extent, use command MEASure:TRIGger:DELay."""
		param = Conversions.enum_scalar_to_str(measurement_mode, enums.MeasurementMode)
		self._core.io.write(f'MEASure:MODE {param}')

	# noinspection PyTypeChecker
	class Result(StructBase):  # From ReadStructDefinition CmdPropertyTemplate.xml
		"""Structure for reading output parameters. Fields: \n
			- Percent: float: No parameter help available
			- Degrees: float: No parameter help available"""
		__meta_args_list = [
			ArgStruct.scalar_float('Percent'),
			ArgStruct.scalar_float('Degrees')]

		def __init__(self):
			StructBase.__init__(self, self)
			self.Percent: float = None
			self.Degrees: float = None

	def get_accuracy(self) -> Result:
		"""SCPI: MEASure:ACCuracy \n
		Snippet: value: Result = driver.measure.get_accuracy() \n
		Queries the accuracy of the last measurement. The R&S LCX returns the accuracy of the impedance (δ|Z|) in percent, and
		the phase angle (δΘ) in degrees. \n
			:return: structure: for return value, see the help for Result structure arguments.
		"""
		return self._core.io.query_struct('MEASure:ACCuracy?', self.__class__.Result())
