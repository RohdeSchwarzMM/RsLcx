from ...Internal.Core import Core
from ...Internal.CommandsGroup import CommandsGroup
from ...Internal import Conversions


# noinspection PyPep8Naming,PyAttributeOutsideInit,SpellCheckingInspection
class TriggerCls:
	"""Trigger commands group definition. 1 total commands, 0 Subgroups, 1 group commands"""

	def __init__(self, core: Core, parent):
		self._core = core
		self._cmd_group = CommandsGroup("trigger", core, parent)

	def get_delay(self) -> float:
		"""SCPI: MEASure:TRIGger:DELay \n
		Snippet: value: float = driver.measure.trigger.get_delay() \n
		Sets a delay time that elapses after a trigger event before the measurement starts. \n
			:return: trigger_delay: No help available
		"""
		response = self._core.io.query_str('MEASure:TRIGger:DELay?')
		return Conversions.str_to_float(response)

	def set_delay(self, trigger_delay: float) -> None:
		"""SCPI: MEASure:TRIGger:DELay \n
		Snippet: driver.measure.trigger.set_delay(trigger_delay = 1.0) \n
		Sets a delay time that elapses after a trigger event before the measurement starts. \n
			:param trigger_delay: No help available
		"""
		param = Conversions.decimal_value_to_str(trigger_delay)
		self._core.io.write(f'MEASure:TRIGger:DELay {param}')
