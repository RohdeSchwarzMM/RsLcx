from ...Internal.Core import Core
from ...Internal.CommandsGroup import CommandsGroup
from ...Internal.Utilities import trim_str_response


# noinspection PyPep8Naming,PyAttributeOutsideInit,SpellCheckingInspection
class SystemCls:
	"""System commands group definition. 26 total commands, 11 Subgroups, 1 group commands"""

	def __init__(self, core: Core, parent):
		self._core = core
		self._cmd_group = CommandsGroup("system", core, parent)

	@property
	def hw(self):
		"""hw commands group. 0 Sub-classes, 1 commands."""
		if not hasattr(self, '_hw'):
			from .Hw import HwCls
			self._hw = HwCls(self._core, self._cmd_group)
		return self._hw

	@property
	def beeper(self):
		"""beeper commands group. 2 Sub-classes, 0 commands."""
		if not hasattr(self, '_beeper'):
			from .Beeper import BeeperCls
			self._beeper = BeeperCls(self._core, self._cmd_group)
		return self._beeper

	@property
	def date(self):
		"""date commands group. 0 Sub-classes, 1 commands."""
		if not hasattr(self, '_date'):
			from .Date import DateCls
			self._date = DateCls(self._core, self._cmd_group)
		return self._date

	@property
	def key(self):
		"""key commands group. 0 Sub-classes, 1 commands."""
		if not hasattr(self, '_key'):
			from .Key import KeyCls
			self._key = KeyCls(self._core, self._cmd_group)
		return self._key

	@property
	def local(self):
		"""local commands group. 0 Sub-classes, 1 commands."""
		if not hasattr(self, '_local'):
			from .Local import LocalCls
			self._local = LocalCls(self._core, self._cmd_group)
		return self._local

	@property
	def remote(self):
		"""remote commands group. 0 Sub-classes, 1 commands."""
		if not hasattr(self, '_remote'):
			from .Remote import RemoteCls
			self._remote = RemoteCls(self._core, self._cmd_group)
		return self._remote

	@property
	def rwLock(self):
		"""rwLock commands group. 0 Sub-classes, 1 commands."""
		if not hasattr(self, '_rwLock'):
			from .RwLock import RwLockCls
			self._rwLock = RwLockCls(self._core, self._cmd_group)
		return self._rwLock

	@property
	def time(self):
		"""time commands group. 0 Sub-classes, 1 commands."""
		if not hasattr(self, '_time'):
			from .Time import TimeCls
			self._time = TimeCls(self._core, self._cmd_group)
		return self._time

	@property
	def restart(self):
		"""restart commands group. 0 Sub-classes, 1 commands."""
		if not hasattr(self, '_restart'):
			from .Restart import RestartCls
			self._restart = RestartCls(self._core, self._cmd_group)
		return self._restart

	@property
	def setting(self):
		"""setting commands group. 1 Sub-classes, 0 commands."""
		if not hasattr(self, '_setting'):
			from .Setting import SettingCls
			self._setting = SettingCls(self._core, self._cmd_group)
		return self._setting

	@property
	def communicate(self):
		"""communicate commands group. 2 Sub-classes, 0 commands."""
		if not hasattr(self, '_communicate'):
			from .Communicate import CommunicateCls
			self._communicate = CommunicateCls(self._core, self._cmd_group)
		return self._communicate

	def get_up_time(self) -> str:
		"""SCPI: SYSTem:UPTime \n
		Snippet: value: str = driver.system.get_up_time() \n
		Queries the up time of the operating system. \n
			:return: result: No help available
		"""
		response = self._core.io.query_str('SYSTem:UPTime?')
		return trim_str_response(response)
