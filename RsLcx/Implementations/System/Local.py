from ...Internal.Core import Core
from ...Internal.CommandsGroup import CommandsGroup


# noinspection PyPep8Naming,PyAttributeOutsideInit,SpellCheckingInspection
class LocalCls:
	"""Local commands group definition. 1 total commands, 0 Subgroups, 1 group commands"""

	def __init__(self, core: Core, parent):
		self._core = core
		self._cmd_group = CommandsGroup("local", core, parent)

	def set(self) -> None:
		"""SCPI: SYSTem:LOCal \n
		Snippet: driver.system.local.set() \n
		Enables manual operation, i.e. unlocks front panel control. To lock manual control, use command method RsLcx.System.
		RwLock.set. \n
		"""
		self._core.io.write(f'SYSTem:LOCal')

	def set_with_opc(self, opc_timeout_ms: int = -1) -> None:
		"""SCPI: SYSTem:LOCal \n
		Snippet: driver.system.local.set_with_opc() \n
		Enables manual operation, i.e. unlocks front panel control. To lock manual control, use command method RsLcx.System.
		RwLock.set. \n
		Same as set, but waits for the operation to complete before continuing further. Use the RsLcx.utilities.opc_timeout_set() to set the timeout value. \n
			:param opc_timeout_ms: Maximum time to wait in milliseconds, valid only for this call."""
		self._core.io.write_with_opc(f'SYSTem:LOCal', opc_timeout_ms)
