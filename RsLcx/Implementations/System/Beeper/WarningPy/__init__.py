from .....Internal.Core import Core
from .....Internal.CommandsGroup import CommandsGroup
from .....Internal import Conversions


# noinspection PyPep8Naming,PyAttributeOutsideInit,SpellCheckingInspection
class WarningPyCls:
	"""WarningPy commands group definition. 2 total commands, 1 Subgroups, 1 group commands"""

	def __init__(self, core: Core, parent):
		self._core = core
		self._cmd_group = CommandsGroup("warningPy", core, parent)

	@property
	def immediate(self):
		"""immediate commands group. 0 Sub-classes, 1 commands."""
		if not hasattr(self, '_immediate'):
			from .Immediate import ImmediateCls
			self._immediate = ImmediateCls(self._core, self._cmd_group)
		return self._immediate

	def get_state(self) -> bool:
		"""SCPI: SYSTem:BEEPer:WARNing:STATe \n
		Snippet: value: bool = driver.system.beeper.warningPy.get_state() \n
		Activates the R&S LCX to create an acoustic signal on errors and warnings. The query returns the current state. \n
			:return: enable: No help available
		"""
		response = self._core.io.query_str('SYSTem:BEEPer:WARNing:STATe?')
		return Conversions.str_to_bool(response)

	def set_state(self, enable: bool) -> None:
		"""SCPI: SYSTem:BEEPer:WARNing:STATe \n
		Snippet: driver.system.beeper.warningPy.set_state(enable = False) \n
		Activates the R&S LCX to create an acoustic signal on errors and warnings. The query returns the current state. \n
			:param enable: No help available
		"""
		param = Conversions.bool_to_str(enable)
		self._core.io.write(f'SYSTem:BEEPer:WARNing:STATe {param}')
