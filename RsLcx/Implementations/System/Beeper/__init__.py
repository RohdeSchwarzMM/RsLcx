from ....Internal.Core import Core
from ....Internal.CommandsGroup import CommandsGroup


# noinspection PyPep8Naming,PyAttributeOutsideInit,SpellCheckingInspection
class BeeperCls:
	"""Beeper commands group definition. 4 total commands, 2 Subgroups, 0 group commands"""

	def __init__(self, core: Core, parent):
		self._core = core
		self._cmd_group = CommandsGroup("beeper", core, parent)

	@property
	def complete(self):
		"""complete commands group. 1 Sub-classes, 1 commands."""
		if not hasattr(self, '_complete'):
			from .Complete import CompleteCls
			self._complete = CompleteCls(self._core, self._cmd_group)
		return self._complete

	@property
	def warningPy(self):
		"""warningPy commands group. 1 Sub-classes, 1 commands."""
		if not hasattr(self, '_warningPy'):
			from .WarningPy import WarningPyCls
			self._warningPy = WarningPyCls(self._core, self._cmd_group)
		return self._warningPy
