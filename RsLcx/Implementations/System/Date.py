from ...Internal.Core import Core
from ...Internal.CommandsGroup import CommandsGroup
from ...Internal.Types import DataType
from ...Internal.StructBase import StructBase
from ...Internal.ArgStruct import ArgStruct
from ...Internal.ArgSingleList import ArgSingleList
from ...Internal.ArgSingle import ArgSingle


# noinspection PyPep8Naming,PyAttributeOutsideInit,SpellCheckingInspection
class DateCls:
	"""Date commands group definition. 1 total commands, 0 Subgroups, 1 group commands"""

	def __init__(self, core: Core, parent):
		self._core = core
		self._cmd_group = CommandsGroup("date", core, parent)

	def set(self, year: float, month: float, day: float) -> None:
		"""SCPI: SYSTem:DATE \n
		Snippet: driver.system.date.set(year = 1.0, month = 1.0, day = 1.0) \n
		Sets or queries the date for the instrument-internal calendar. \n
			:param year: Sets the year.
			:param month: Sets the month.
			:param day: Sets the day.
		"""
		param = ArgSingleList().compose_cmd_string(ArgSingle('year', year, DataType.Float), ArgSingle('month', month, DataType.Float), ArgSingle('day', day, DataType.Float))
		self._core.io.write(f'SYSTem:DATE {param}'.rstrip())

	# noinspection PyTypeChecker
	class DateStruct(StructBase):
		"""Response structure. Fields: \n
			- Year: float: Sets the year.
			- Month: float: Sets the month.
			- Day: float: Sets the day."""
		__meta_args_list = [
			ArgStruct.scalar_float('Year'),
			ArgStruct.scalar_float('Month'),
			ArgStruct.scalar_float('Day')]

		def __init__(self):
			StructBase.__init__(self, self)
			self.Year: float = None
			self.Month: float = None
			self.Day: float = None

	def get(self) -> DateStruct:
		"""SCPI: SYSTem:DATE \n
		Snippet: value: DateStruct = driver.system.date.get() \n
		Sets or queries the date for the instrument-internal calendar. \n
			:return: structure: for return value, see the help for DateStruct structure arguments."""
		return self._core.io.query_struct(f'SYSTem:DATE?', self.__class__.DateStruct())
