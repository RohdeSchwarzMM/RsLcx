from ...Internal.Core import Core
from ...Internal.CommandsGroup import CommandsGroup
from ...Internal.Utilities import trim_str_response


# noinspection PyPep8Naming,PyAttributeOutsideInit,SpellCheckingInspection
class HwCls:
	"""Hw commands group definition. 1 total commands, 0 Subgroups, 1 group commands"""

	def __init__(self, core: Core, parent):
		self._core = core
		self._cmd_group = CommandsGroup("hw", core, parent)

	def get_version(self) -> str:
		"""SCPI: SYSTem:HW:VERSion \n
		Snippet: value: str = driver.system.hw.get_version() \n
		Queries the hardware version of the instrument. \n
			:return: result: No help available
		"""
		response = self._core.io.query_str('SYSTem:HW:VERSion?')
		return trim_str_response(response)
