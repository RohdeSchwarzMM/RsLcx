from ...Internal.Core import Core
from ...Internal.CommandsGroup import CommandsGroup


# noinspection PyPep8Naming,PyAttributeOutsideInit,SpellCheckingInspection
class RemoteCls:
	"""Remote commands group definition. 1 total commands, 0 Subgroups, 1 group commands"""

	def __init__(self, core: Core, parent):
		self._core = core
		self._cmd_group = CommandsGroup("remote", core, parent)

	def set(self) -> None:
		"""SCPI: SYSTem:REMote \n
		Snippet: driver.system.remote.set() \n
		Activates remote control. The R&S LCX switches to remote state, and locks all front panel controls. You can control the
		R&S LCX remotely. Sending a command sets the instrument to remote state, indicated by the white SCPI icon in the status
		bar. To return to manual control, use command method RsLcx.System.Local.set. \n
		"""
		self._core.io.write(f'SYSTem:REMote')

	def set_with_opc(self, opc_timeout_ms: int = -1) -> None:
		"""SCPI: SYSTem:REMote \n
		Snippet: driver.system.remote.set_with_opc() \n
		Activates remote control. The R&S LCX switches to remote state, and locks all front panel controls. You can control the
		R&S LCX remotely. Sending a command sets the instrument to remote state, indicated by the white SCPI icon in the status
		bar. To return to manual control, use command method RsLcx.System.Local.set. \n
		Same as set, but waits for the operation to complete before continuing further. Use the RsLcx.utilities.opc_timeout_set() to set the timeout value. \n
			:param opc_timeout_ms: Maximum time to wait in milliseconds, valid only for this call."""
		self._core.io.write_with_opc(f'SYSTem:REMote', opc_timeout_ms)
