from ....Internal.Core import Core
from ....Internal.CommandsGroup import CommandsGroup


# noinspection PyPep8Naming,PyAttributeOutsideInit,SpellCheckingInspection
class CommunicateCls:
	"""Communicate commands group definition. 12 total commands, 2 Subgroups, 0 group commands"""

	def __init__(self, core: Core, parent):
		self._core = core
		self._cmd_group = CommandsGroup("communicate", core, parent)

	@property
	def lan(self):
		"""lan commands group. 2 Sub-classes, 8 commands."""
		if not hasattr(self, '_lan'):
			from .Lan import LanCls
			self._lan = LanCls(self._core, self._cmd_group)
		return self._lan

	@property
	def network(self):
		"""network commands group. 1 Sub-classes, 0 commands."""
		if not hasattr(self, '_network'):
			from .Network import NetworkCls
			self._network = NetworkCls(self._core, self._cmd_group)
		return self._network
