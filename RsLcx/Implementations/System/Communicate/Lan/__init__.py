from .....Internal.Core import Core
from .....Internal.CommandsGroup import CommandsGroup
from .....Internal import Conversions
from .....Internal.Utilities import trim_str_response


# noinspection PyPep8Naming,PyAttributeOutsideInit,SpellCheckingInspection
class LanCls:
	"""Lan commands group definition. 10 total commands, 2 Subgroups, 8 group commands"""

	def __init__(self, core: Core, parent):
		self._core = core
		self._cmd_group = CommandsGroup("lan", core, parent)

	@property
	def apply(self):
		"""apply commands group. 0 Sub-classes, 1 commands."""
		if not hasattr(self, '_apply'):
			from .Apply import ApplyCls
			self._apply = ApplyCls(self._core, self._cmd_group)
		return self._apply

	@property
	def discard(self):
		"""discard commands group. 0 Sub-classes, 1 commands."""
		if not hasattr(self, '_discard'):
			from .Discard import DiscardCls
			self._discard = DiscardCls(self._core, self._cmd_group)
		return self._discard

	def get_dhcp(self) -> bool:
		"""SCPI: SYSTem:COMMunicate:LAN:DHCP \n
		Snippet: value: bool = driver.system.communicate.lan.get_dhcp() \n
		No command help available \n
			:return: enable: No help available
		"""
		response = self._core.io.query_str('SYSTem:COMMunicate:LAN:DHCP?')
		return Conversions.str_to_bool(response)

	def set_dhcp(self, enable: bool) -> None:
		"""SCPI: SYSTem:COMMunicate:LAN:DHCP \n
		Snippet: driver.system.communicate.lan.set_dhcp(enable = False) \n
		No command help available \n
			:param enable: No help available
		"""
		param = Conversions.bool_to_str(enable)
		self._core.io.write(f'SYSTem:COMMunicate:LAN:DHCP {param}')

	def get_address(self) -> str:
		"""SCPI: SYSTem:COMMunicate:LAN:ADDRess \n
		Snippet: value: str = driver.system.communicate.lan.get_address() \n
		Sets the IP address. \n
			:return: ip_address: No help available
		"""
		response = self._core.io.query_str('SYSTem:COMMunicate:LAN:ADDRess?')
		return trim_str_response(response)

	def set_address(self, ip_address: str) -> None:
		"""SCPI: SYSTem:COMMunicate:LAN:ADDRess \n
		Snippet: driver.system.communicate.lan.set_address(ip_address = '1') \n
		Sets the IP address. \n
			:param ip_address: No help available
		"""
		param = Conversions.value_to_quoted_str(ip_address)
		self._core.io.write(f'SYSTem:COMMunicate:LAN:ADDRess {param}')

	def get_smask(self) -> str:
		"""SCPI: SYSTem:COMMunicate:LAN:SMASk \n
		Snippet: value: str = driver.system.communicate.lan.get_smask() \n
		Sets the subnet mask. \n
			:return: subnet_mask: No help available
		"""
		response = self._core.io.query_str('SYSTem:COMMunicate:LAN:SMASk?')
		return trim_str_response(response)

	def set_smask(self, subnet_mask: str) -> None:
		"""SCPI: SYSTem:COMMunicate:LAN:SMASk \n
		Snippet: driver.system.communicate.lan.set_smask(subnet_mask = '1') \n
		Sets the subnet mask. \n
			:param subnet_mask: No help available
		"""
		param = Conversions.value_to_quoted_str(subnet_mask)
		self._core.io.write(f'SYSTem:COMMunicate:LAN:SMASk {param}')

	def get_dgateway(self) -> str:
		"""SCPI: SYSTem:COMMunicate:LAN:DGATeway \n
		Snippet: value: str = driver.system.communicate.lan.get_dgateway() \n
		Sets the IP address of the default gateway. \n
			:return: gateway: No help available
		"""
		response = self._core.io.query_str('SYSTem:COMMunicate:LAN:DGATeway?')
		return trim_str_response(response)

	def set_dgateway(self, gateway: str) -> None:
		"""SCPI: SYSTem:COMMunicate:LAN:DGATeway \n
		Snippet: driver.system.communicate.lan.set_dgateway(gateway = '1') \n
		Sets the IP address of the default gateway. \n
			:param gateway: No help available
		"""
		param = Conversions.value_to_quoted_str(gateway)
		self._core.io.write(f'SYSTem:COMMunicate:LAN:DGATeway {param}')

	def get_hostname(self) -> str:
		"""SCPI: SYSTem:COMMunicate:LAN:HOSTname \n
		Snippet: value: str = driver.system.communicate.lan.get_hostname() \n
		Sets an individual hostname for the R&S LCX. \n
			:return: device_hostname: No help available
		"""
		response = self._core.io.query_str('SYSTem:COMMunicate:LAN:HOSTname?')
		return trim_str_response(response)

	def set_hostname(self, device_hostname: str) -> None:
		"""SCPI: SYSTem:COMMunicate:LAN:HOSTname \n
		Snippet: driver.system.communicate.lan.set_hostname(device_hostname = '1') \n
		Sets an individual hostname for the R&S LCX. \n
			:param device_hostname: No help available
		"""
		param = Conversions.value_to_quoted_str(device_hostname)
		self._core.io.write(f'SYSTem:COMMunicate:LAN:HOSTname {param}')

	def get_mac(self) -> str:
		"""SCPI: SYSTem:COMMunicate:LAN:MAC \n
		Snippet: value: str = driver.system.communicate.lan.get_mac() \n
		Queries the MAC address of the network. \n
			:return: result: No help available
		"""
		response = self._core.io.query_str('SYSTem:COMMunicate:LAN:MAC?')
		return trim_str_response(response)

	def reset(self) -> None:
		"""SCPI: SYSTem:COMMunicate:LAN:RESet \n
		Snippet: driver.system.communicate.lan.reset() \n
		Terminates the network configuration and restarts the network. \n
		"""
		self._core.io.write(f'SYSTem:COMMunicate:LAN:RESet')

	def reset_with_opc(self, opc_timeout_ms: int = -1) -> None:
		"""SCPI: SYSTem:COMMunicate:LAN:RESet \n
		Snippet: driver.system.communicate.lan.reset_with_opc() \n
		Terminates the network configuration and restarts the network. \n
		Same as reset, but waits for the operation to complete before continuing further. Use the RsLcx.utilities.opc_timeout_set() to set the timeout value. \n
			:param opc_timeout_ms: Maximum time to wait in milliseconds, valid only for this call."""
		self._core.io.write_with_opc(f'SYSTem:COMMunicate:LAN:RESet', opc_timeout_ms)

	def get_edited(self) -> bool:
		"""SCPI: SYSTem:COMMunicate:LAN:EDITed \n
		Snippet: value: bool = driver.system.communicate.lan.get_edited() \n
		No command help available \n
			:return: result: No help available
		"""
		response = self._core.io.query_str('SYSTem:COMMunicate:LAN:EDITed?')
		return Conversions.str_to_bool(response)
