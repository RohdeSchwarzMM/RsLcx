from .....Internal.Core import Core
from .....Internal.CommandsGroup import CommandsGroup


# noinspection PyPep8Naming,PyAttributeOutsideInit,SpellCheckingInspection
class NetworkCls:
	"""Network commands group definition. 2 total commands, 1 Subgroups, 0 group commands"""

	def __init__(self, core: Core, parent):
		self._core = core
		self._cmd_group = CommandsGroup("network", core, parent)

	@property
	def vnc(self):
		"""vnc commands group. 0 Sub-classes, 2 commands."""
		if not hasattr(self, '_vnc'):
			from .Vnc import VncCls
			self._vnc = VncCls(self._core, self._cmd_group)
		return self._vnc
