from .....Internal.Core import Core
from .....Internal.CommandsGroup import CommandsGroup
from .....Internal import Conversions


# noinspection PyPep8Naming,PyAttributeOutsideInit,SpellCheckingInspection
class VncCls:
	"""Vnc commands group definition. 2 total commands, 0 Subgroups, 2 group commands"""

	def __init__(self, core: Core, parent):
		self._core = core
		self._cmd_group = CommandsGroup("vnc", core, parent)

	def get_state(self) -> bool:
		"""SCPI: SYSTem:COMMunicate:NETWork:VNC[:STATe] \n
		Snippet: value: bool = driver.system.communicate.network.vnc.get_state() \n
		Activates the VNC interface for remote access. \n
			:return: enable: No help available
		"""
		response = self._core.io.query_str('SYSTem:COMMunicate:NETWork:VNC:STATe?')
		return Conversions.str_to_bool(response)

	def set_state(self, enable: bool) -> None:
		"""SCPI: SYSTem:COMMunicate:NETWork:VNC[:STATe] \n
		Snippet: driver.system.communicate.network.vnc.set_state(enable = False) \n
		Activates the VNC interface for remote access. \n
			:param enable: No help available
		"""
		param = Conversions.bool_to_str(enable)
		self._core.io.write(f'SYSTem:COMMunicate:NETWork:VNC:STATe {param}')

	def get_port(self) -> int:
		"""SCPI: SYSTem:COMMunicate:NETWork:VNC:PORT \n
		Snippet: value: int = driver.system.communicate.network.vnc.get_port() \n
		Sets the VNC port address. \n
			:return: port: No help available
		"""
		response = self._core.io.query_str('SYSTem:COMMunicate:NETWork:VNC:PORT?')
		return Conversions.str_to_int(response)

	def set_port(self, port: int) -> None:
		"""SCPI: SYSTem:COMMunicate:NETWork:VNC:PORT \n
		Snippet: driver.system.communicate.network.vnc.set_port(port = 1) \n
		Sets the VNC port address. \n
			:param port: No help available
		"""
		param = Conversions.decimal_value_to_str(port)
		self._core.io.write(f'SYSTem:COMMunicate:NETWork:VNC:PORT {param}')
