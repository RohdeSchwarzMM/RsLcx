from ....Internal.Core import Core
from ....Internal.CommandsGroup import CommandsGroup
from ....Internal import Conversions


# noinspection PyPep8Naming,PyAttributeOutsideInit,SpellCheckingInspection
class DefaultCls:
	"""Default commands group definition. 1 total commands, 0 Subgroups, 1 group commands"""

	def __init__(self, core: Core, parent):
		self._core = core
		self._cmd_group = CommandsGroup("default", core, parent)

	def save(self, file_path: str = None) -> None:
		"""SCPI: SYSTem:SETTing:DEFault:SAVE \n
		Snippet: driver.system.setting.default.save(file_path = '1') \n
		Saves the current instrument settings in a file with defined filename. To recall an instrument configuration, use command
		*RCL. \n
			:param file_path: No help available
		"""
		param = ''
		if file_path:
			param = Conversions.value_to_quoted_str(file_path)
		self._core.io.write(f'SYSTem:SETTing:DEFault:SAVE {param}'.strip())
