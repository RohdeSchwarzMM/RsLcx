from ...Internal.Core import Core
from ...Internal.CommandsGroup import CommandsGroup
from ...Internal import Conversions
from ... import enums


# noinspection PyPep8Naming,PyAttributeOutsideInit,SpellCheckingInspection
class UsbCls:
	"""Usb commands group definition. 1 total commands, 0 Subgroups, 1 group commands"""

	def __init__(self, core: Core, parent):
		self._core = core
		self._cmd_group = CommandsGroup("usb", core, parent)

	# noinspection PyTypeChecker
	def get_class_py(self) -> enums.UsbClass:
		"""SCPI: INTerfaces:USB:CLASs \n
		Snippet: value: enums.UsbClass = driver.interfaces.usb.get_class_py() \n
		Selects the USB communication class. \n
			:return: usb_class:
				- CDC: Uses the virtual communication port protocol, that enables you to emulate serial ports over USB.
				- TMC: Uses the protocol for communication with USB devices."""
		response = self._core.io.query_str('INTerfaces:USB:CLASs?')
		return Conversions.str_to_scalar_enum(response, enums.UsbClass)

	def set_class_py(self, usb_class: enums.UsbClass) -> None:
		"""SCPI: INTerfaces:USB:CLASs \n
		Snippet: driver.interfaces.usb.set_class_py(usb_class = enums.UsbClass.CDC) \n
		Selects the USB communication class. \n
			:param usb_class:
				- CDC: Uses the virtual communication port protocol, that enables you to emulate serial ports over USB.
				- TMC: Uses the protocol for communication with USB devices."""
		param = Conversions.enum_scalar_to_str(usb_class, enums.UsbClass)
		self._core.io.write(f'INTerfaces:USB:CLASs {param}')
