from ....Internal.Core import Core
from ....Internal.CommandsGroup import CommandsGroup
from ....Internal import Conversions
from ....Internal.Utilities import trim_str_response


# noinspection PyPep8Naming,PyAttributeOutsideInit,SpellCheckingInspection
class ShortCls:
	"""Short commands group definition. 3 total commands, 1 Subgroups, 2 group commands"""

	def __init__(self, core: Core, parent):
		self._core = core
		self._cmd_group = CommandsGroup("short", core, parent)

	@property
	def execute(self):
		"""execute commands group. 0 Sub-classes, 1 commands."""
		if not hasattr(self, '_execute'):
			from .Execute import ExecuteCls
			self._execute = ExecuteCls(self._core, self._cmd_group)
		return self._execute

	def get_state(self) -> bool:
		"""SCPI: CORRection:SHORt:STATe \n
		Snippet: value: bool = driver.correction.short.get_state() \n
		Activates the short correction function. \n
			:return: short_correction_state: No help available
		"""
		response = self._core.io.query_str('CORRection:SHORt:STATe?')
		return Conversions.str_to_bool(response)

	def set_state(self, short_correction_state: bool) -> None:
		"""SCPI: CORRection:SHORt:STATe \n
		Snippet: driver.correction.short.set_state(short_correction_state = False) \n
		Activates the short correction function. \n
			:param short_correction_state: No help available
		"""
		param = Conversions.bool_to_str(short_correction_state)
		self._core.io.write(f'CORRection:SHORt:STATe {param}')

	def get_mode(self) -> str:
		"""SCPI: CORRection:SHORt:MODE \n
		Snippet: value: str = driver.correction.short.get_mode() \n
		No command help available \n
			:return: result: No help available
		"""
		response = self._core.io.query_str('CORRection:SHORt:MODE?')
		return trim_str_response(response)
