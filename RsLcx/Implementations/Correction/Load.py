from ...Internal.Core import Core
from ...Internal.CommandsGroup import CommandsGroup
from ...Internal import Conversions
from ...Internal.Utilities import trim_str_response


# noinspection PyPep8Naming,PyAttributeOutsideInit,SpellCheckingInspection
class LoadCls:
	"""Load commands group definition. 2 total commands, 0 Subgroups, 2 group commands"""

	def __init__(self, core: Core, parent):
		self._core = core
		self._cmd_group = CommandsGroup("load", core, parent)

	def get_state(self) -> bool:
		"""SCPI: CORRection:LOAD:STATe \n
		Snippet: value: bool = driver.correction.load.get_state() \n
		Activates the load correction function. \n
			:return: load_correction_state: No help available
		"""
		response = self._core.io.query_str('CORRection:LOAD:STATe?')
		return Conversions.str_to_bool(response)

	def set_state(self, load_correction_state: bool) -> None:
		"""SCPI: CORRection:LOAD:STATe \n
		Snippet: driver.correction.load.set_state(load_correction_state = False) \n
		Activates the load correction function. \n
			:param load_correction_state: No help available
		"""
		param = Conversions.bool_to_str(load_correction_state)
		self._core.io.write(f'CORRection:LOAD:STATe {param}')

	def get_mode(self) -> str:
		"""SCPI: CORRection:LOAD:MODE \n
		Snippet: value: str = driver.correction.load.get_mode() \n
		No command help available \n
			:return: result: No help available
		"""
		response = self._core.io.query_str('CORRection:LOAD:MODE?')
		return trim_str_response(response)
