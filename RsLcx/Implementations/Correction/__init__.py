from ...Internal.Core import Core
from ...Internal.CommandsGroup import CommandsGroup
from ...Internal import Conversions


# noinspection PyPep8Naming,PyAttributeOutsideInit,SpellCheckingInspection
class CorrectionCls:
	"""Correction commands group definition. 13 total commands, 4 Subgroups, 1 group commands"""

	def __init__(self, core: Core, parent):
		self._core = core
		self._cmd_group = CommandsGroup("correction", core, parent)

	@property
	def open(self):
		"""open commands group. 1 Sub-classes, 2 commands."""
		if not hasattr(self, '_open'):
			from .Open import OpenCls
			self._open = OpenCls(self._core, self._cmd_group)
		return self._open

	@property
	def short(self):
		"""short commands group. 1 Sub-classes, 2 commands."""
		if not hasattr(self, '_short'):
			from .Short import ShortCls
			self._short = ShortCls(self._core, self._cmd_group)
		return self._short

	@property
	def load(self):
		"""load commands group. 0 Sub-classes, 2 commands."""
		if not hasattr(self, '_load'):
			from .Load import LoadCls
			self._load = LoadCls(self._core, self._cmd_group)
		return self._load

	@property
	def spot(self):
		"""spot commands group. 3 Sub-classes, 0 commands."""
		if not hasattr(self, '_spot'):
			from .Spot import SpotCls
			self._spot = SpotCls(self._core, self._cmd_group)
		return self._spot

	def get_length(self) -> float:
		"""SCPI: CORRection:LENGth \n
		Snippet: value: float = driver.correction.get_length() \n
		Sets the length of the leads to the connected test fixture i.e. the DUT. \n
			:return: cable_length: No help available
		"""
		response = self._core.io.query_str('CORRection:LENGth?')
		return Conversions.str_to_float(response)

	def set_length(self, cable_length: float) -> None:
		"""SCPI: CORRection:LENGth \n
		Snippet: driver.correction.set_length(cable_length = 1.0) \n
		Sets the length of the leads to the connected test fixture i.e. the DUT. \n
			:param cable_length: No help available
		"""
		param = Conversions.decimal_value_to_str(cable_length)
		self._core.io.write(f'CORRection:LENGth {param}')
