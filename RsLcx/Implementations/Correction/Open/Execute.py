from ....Internal.Core import Core
from ....Internal.CommandsGroup import CommandsGroup


# noinspection PyPep8Naming,PyAttributeOutsideInit,SpellCheckingInspection
class ExecuteCls:
	"""Execute commands group definition. 1 total commands, 0 Subgroups, 1 group commands"""

	def __init__(self, core: Core, parent):
		self._core = core
		self._cmd_group = CommandsGroup("execute", core, parent)

	def set(self, opc_timeout_ms: int = -1) -> None:
		"""SCPI: CORRection:OPEN[:EXECute] \n
		Snippet: driver.correction.open.execute.set() \n
		Executes an open correction on all frequencies. \n
			:param opc_timeout_ms: Maximum time to wait in milliseconds, valid only for this call."""
		self._core.io.write_with_opc(f'CORRection:OPEN:EXECute', opc_timeout_ms)
