from .....Internal.Core import Core
from .....Internal.CommandsGroup import CommandsGroup
from ..... import repcap


# noinspection PyPep8Naming,PyAttributeOutsideInit,SpellCheckingInspection
class ExecuteCls:
	"""Execute commands group definition. 1 total commands, 0 Subgroups, 1 group commands"""

	def __init__(self, core: Core, parent):
		self._core = core
		self._cmd_group = CommandsGroup("execute", core, parent)

	def set(self, spot=repcap.Spot.Default, opc_timeout_ms: int = -1) -> None:
		"""SCPI: CORRection:SPOT<Spot>:SHORt[:EXECute] \n
		Snippet: driver.correction.spot.short.execute.set(spot = repcap.Spot.Default) \n
		Executes a short correction at a dedicated working point. \n
			:param spot: optional repeated capability selector. Default value: Nr1 (settable in the interface 'Spot')
			:param opc_timeout_ms: Maximum time to wait in milliseconds, valid only for this call."""
		spot_cmd_val = self._cmd_group.get_repcap_cmd_value(spot, repcap.Spot)
		self._core.io.write_with_opc(f'CORRection:SPOT{spot_cmd_val}:SHORt:EXECute', opc_timeout_ms)
