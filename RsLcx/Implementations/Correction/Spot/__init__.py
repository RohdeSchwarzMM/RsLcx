from ....Internal.Core import Core
from ....Internal.CommandsGroup import CommandsGroup
from ....Internal.RepeatedCapability import RepeatedCapability
from .... import repcap


# noinspection PyPep8Naming,PyAttributeOutsideInit,SpellCheckingInspection
class SpotCls:
	"""Spot commands group definition. 4 total commands, 3 Subgroups, 0 group commands
	Repeated Capability: Spot, default value after init: Spot.Nr1"""

	def __init__(self, core: Core, parent):
		self._core = core
		self._cmd_group = CommandsGroup("spot", core, parent)
		self._cmd_group.rep_cap = RepeatedCapability(self._cmd_group.group_name, 'repcap_spot_get', 'repcap_spot_set', repcap.Spot.Nr1)

	def repcap_spot_set(self, spot: repcap.Spot) -> None:
		"""Repeated Capability default value numeric suffix.
		This value is used, if you do not explicitely set it in the child set/get methods, or if you leave it to Spot.Default
		Default value after init: Spot.Nr1"""
		self._cmd_group.set_repcap_enum_value(spot)

	def repcap_spot_get(self) -> repcap.Spot:
		"""Returns the current default repeated capability for the child set/get methods"""
		# noinspection PyTypeChecker
		return self._cmd_group.get_repcap_enum_value()

	@property
	def open(self):
		"""open commands group. 1 Sub-classes, 0 commands."""
		if not hasattr(self, '_open'):
			from .Open import OpenCls
			self._open = OpenCls(self._core, self._cmd_group)
		return self._open

	@property
	def short(self):
		"""short commands group. 1 Sub-classes, 0 commands."""
		if not hasattr(self, '_short'):
			from .Short import ShortCls
			self._short = ShortCls(self._core, self._cmd_group)
		return self._short

	@property
	def load(self):
		"""load commands group. 2 Sub-classes, 0 commands."""
		if not hasattr(self, '_load'):
			from .Load import LoadCls
			self._load = LoadCls(self._core, self._cmd_group)
		return self._load
