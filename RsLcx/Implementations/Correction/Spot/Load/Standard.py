from .....Internal.Core import Core
from .....Internal.CommandsGroup import CommandsGroup
from .....Internal.Types import DataType
from .....Internal.StructBase import StructBase
from .....Internal.ArgStruct import ArgStruct
from .....Internal.ArgSingleList import ArgSingleList
from .....Internal.ArgSingle import ArgSingle
from ..... import repcap


# noinspection PyPep8Naming,PyAttributeOutsideInit,SpellCheckingInspection
class StandardCls:
	"""Standard commands group definition. 1 total commands, 0 Subgroups, 1 group commands"""

	def __init__(self, core: Core, parent):
		self._core = core
		self._cmd_group = CommandsGroup("standard", core, parent)

	def set(self, reference_value_for_primary: float, reference_value_for_secondary: float, spot=repcap.Spot.Default) -> None:
		"""SCPI: CORRection:SPOT<Spot>:LOAD:STANdard \n
		Snippet: driver.correction.spot.load.standard.set(reference_value_for_primary = 1.0, reference_value_for_secondary = 1.0, spot = repcap.Spot.Default) \n
		Defines a working point for load correction. Assign the working point number and the primary and secondary reference
		values. \n
			:param reference_value_for_primary: Sets the primary standard value as reference, e.g. the value of a calibration resistor.
			:param reference_value_for_secondary: Sets the secondary standard value.
			:param spot: optional repeated capability selector. Default value: Nr1 (settable in the interface 'Spot')
		"""
		param = ArgSingleList().compose_cmd_string(ArgSingle('reference_value_for_primary', reference_value_for_primary, DataType.Float), ArgSingle('reference_value_for_secondary', reference_value_for_secondary, DataType.Float))
		spot_cmd_val = self._cmd_group.get_repcap_cmd_value(spot, repcap.Spot)
		self._core.io.write(f'CORRection:SPOT{spot_cmd_val}:LOAD:STANdard {param}'.rstrip())

	# noinspection PyTypeChecker
	class StandardStruct(StructBase):
		"""Response structure. Fields: \n
			- Reference_Value_For_Primary: float: Sets the primary standard value as reference, e.g. the value of a calibration resistor.
			- Reference_Value_For_Secondary: float: Sets the secondary standard value."""
		__meta_args_list = [
			ArgStruct.scalar_float('Reference_Value_For_Primary'),
			ArgStruct.scalar_float('Reference_Value_For_Secondary')]

		def __init__(self):
			StructBase.__init__(self, self)
			self.Reference_Value_For_Primary: float = None
			self.Reference_Value_For_Secondary: float = None

	def get(self, spot=repcap.Spot.Default) -> StandardStruct:
		"""SCPI: CORRection:SPOT<Spot>:LOAD:STANdard \n
		Snippet: value: StandardStruct = driver.correction.spot.load.standard.get(spot = repcap.Spot.Default) \n
		Defines a working point for load correction. Assign the working point number and the primary and secondary reference
		values. \n
			:param spot: optional repeated capability selector. Default value: Nr1 (settable in the interface 'Spot')
			:return: structure: for return value, see the help for StandardStruct structure arguments."""
		spot_cmd_val = self._cmd_group.get_repcap_cmd_value(spot, repcap.Spot)
		return self._core.io.query_struct(f'CORRection:SPOT{spot_cmd_val}:LOAD:STANdard?', self.__class__.StandardStruct())
