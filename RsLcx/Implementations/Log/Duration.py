from ...Internal.Core import Core
from ...Internal.CommandsGroup import CommandsGroup
from ...Internal import Conversions
from ...Internal.Types import DataType
from ...Internal.ArgSingleList import ArgSingleList
from ...Internal.ArgSingle import ArgSingle
from ... import enums


# noinspection PyPep8Naming,PyAttributeOutsideInit,SpellCheckingInspection
class DurationCls:
	"""Duration commands group definition. 1 total commands, 0 Subgroups, 1 group commands"""

	def __init__(self, core: Core, parent):
		self._core = core
		self._cmd_group = CommandsGroup("duration", core, parent)

	def set(self, logging_duration: float, return_min_or_max: enums.MinOrMax = None) -> None:
		"""SCPI: LOG:DURation \n
		Snippet: driver.log.duration.set(logging_duration = 1.0, return_min_or_max = enums.MinOrMax.MAX) \n
		Defines the duration of logging for the measurement in span and duration mode. To set the mode, use the command method
		RsLcx.Log.mode. \n
			:param logging_duration: Numeric value in seconds.
			:param return_min_or_max: No help available
		"""
		param = ArgSingleList().compose_cmd_string(ArgSingle('logging_duration', logging_duration, DataType.Float), ArgSingle('return_min_or_max', return_min_or_max, DataType.Enum, enums.MinOrMax, is_optional=True))
		self._core.io.write(f'LOG:DURation {param}'.rstrip())

	def get(self) -> float:
		"""SCPI: LOG:DURation \n
		Snippet: value: float = driver.log.duration.get() \n
		Defines the duration of logging for the measurement in span and duration mode. To set the mode, use the command method
		RsLcx.Log.mode. \n
			:return: logging_duration: Numeric value in seconds."""
		response = self._core.io.query_str(f'LOG:DURation?')
		return Conversions.str_to_float(response)
