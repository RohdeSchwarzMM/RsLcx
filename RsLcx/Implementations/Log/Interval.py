from ...Internal.Core import Core
from ...Internal.CommandsGroup import CommandsGroup
from ...Internal import Conversions
from ...Internal.Types import DataType
from ...Internal.ArgSingleList import ArgSingleList
from ...Internal.ArgSingle import ArgSingle
from ... import enums


# noinspection PyPep8Naming,PyAttributeOutsideInit,SpellCheckingInspection
class IntervalCls:
	"""Interval commands group definition. 1 total commands, 0 Subgroups, 1 group commands"""

	def __init__(self, core: Core, parent):
		self._core = core
		self._cmd_group = CommandsGroup("interval", core, parent)

	def set(self, sample_interval: float, return_min_or_max: enums.MinOrMax = None) -> None:
		"""SCPI: LOG:INTerval \n
		Snippet: driver.log.interval.set(sample_interval = 1.0, return_min_or_max = enums.MinOrMax.MAX) \n
		Selects the logging measurement interval. The measurement interval describes the time between the recorded measurements. \n
			:param sample_interval: Numeric value in seconds.
				- 0: Logs a new measurement.
			:param return_min_or_max: No help available"""
		param = ArgSingleList().compose_cmd_string(ArgSingle('sample_interval', sample_interval, DataType.Float), ArgSingle('return_min_or_max', return_min_or_max, DataType.Enum, enums.MinOrMax, is_optional=True))
		self._core.io.write(f'LOG:INTerval {param}'.rstrip())

	def get(self) -> float:
		"""SCPI: LOG:INTerval \n
		Snippet: value: float = driver.log.interval.get() \n
		Selects the logging measurement interval. The measurement interval describes the time between the recorded measurements. \n
			:return: sample_interval: Numeric value in seconds.
				- 0: Logs a new measurement."""
		response = self._core.io.query_str(f'LOG:INTerval?')
		return Conversions.str_to_float(response)
