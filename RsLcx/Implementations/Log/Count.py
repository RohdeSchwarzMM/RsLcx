from ...Internal.Core import Core
from ...Internal.CommandsGroup import CommandsGroup
from ...Internal import Conversions
from ...Internal.Types import DataType
from ...Internal.ArgSingleList import ArgSingleList
from ...Internal.ArgSingle import ArgSingle
from ... import enums


# noinspection PyPep8Naming,PyAttributeOutsideInit,SpellCheckingInspection
class CountCls:
	"""Count commands group definition. 1 total commands, 0 Subgroups, 1 group commands"""

	def __init__(self, core: Core, parent):
		self._core = core
		self._cmd_group = CommandsGroup("count", core, parent)

	def set(self, sample_count: float, return_min_or_max: enums.MinOrMax = None) -> None:
		"""SCPI: LOG:COUNt \n
		Snippet: driver.log.count.set(sample_count = 1.0, return_min_or_max = enums.MinOrMax.MAX) \n
		Sets the number of measurement readings in count mode. To set the mode, use the command method RsLcx.Log.
		mode to be captured. \n
			:param sample_count: No help available
			:param return_min_or_max: No help available
		"""
		param = ArgSingleList().compose_cmd_string(ArgSingle('sample_count', sample_count, DataType.Float), ArgSingle('return_min_or_max', return_min_or_max, DataType.Enum, enums.MinOrMax, is_optional=True))
		self._core.io.write(f'LOG:COUNt {param}'.rstrip())

	def get(self) -> float:
		"""SCPI: LOG:COUNt \n
		Snippet: value: float = driver.log.count.get() \n
		Sets the number of measurement readings in count mode. To set the mode, use the command method RsLcx.Log.
		mode to be captured. \n
			:return: sample_count: No help available"""
		response = self._core.io.query_str(f'LOG:COUNt?')
		return Conversions.str_to_float(response)
