from ...Internal.Core import Core
from ...Internal.CommandsGroup import CommandsGroup
from ...Internal import Conversions
from ...Internal.Utilities import trim_str_response
from ...Internal.StructBase import StructBase
from ...Internal.ArgStruct import ArgStruct
from ... import enums


# noinspection PyPep8Naming,PyAttributeOutsideInit,SpellCheckingInspection
class LogCls:
	"""Log commands group definition. 7 total commands, 3 Subgroups, 4 group commands"""

	def __init__(self, core: Core, parent):
		self._core = core
		self._cmd_group = CommandsGroup("log", core, parent)

	@property
	def count(self):
		"""count commands group. 0 Sub-classes, 1 commands."""
		if not hasattr(self, '_count'):
			from .Count import CountCls
			self._count = CountCls(self._core, self._cmd_group)
		return self._count

	@property
	def duration(self):
		"""duration commands group. 0 Sub-classes, 1 commands."""
		if not hasattr(self, '_duration'):
			from .Duration import DurationCls
			self._duration = DurationCls(self._core, self._cmd_group)
		return self._duration

	@property
	def interval(self):
		"""interval commands group. 0 Sub-classes, 1 commands."""
		if not hasattr(self, '_interval'):
			from .Interval import IntervalCls
			self._interval = IntervalCls(self._core, self._cmd_group)
		return self._interval

	def get_state(self) -> bool:
		"""SCPI: LOG[:STATe] \n
		Snippet: value: bool = driver.log.get_state() \n
		Activates the data logging function. \n
			:return: logging_state: No help available
		"""
		response = self._core.io.query_str('LOG:STATe?')
		return Conversions.str_to_bool(response)

	def set_state(self, logging_state: bool) -> None:
		"""SCPI: LOG[:STATe] \n
		Snippet: driver.log.set_state(logging_state = False) \n
		Activates the data logging function. \n
			:param logging_state: No help available
		"""
		param = Conversions.bool_to_str(logging_state)
		self._core.io.write(f'LOG:STATe {param}')

	# noinspection PyTypeChecker
	def get_mode(self) -> enums.LoggingMode:
		"""SCPI: LOG:MODE \n
		Snippet: value: enums.LoggingMode = driver.log.get_mode() \n
		Selects the data logging mode. \n
			:return: logging_mode:
				- UNLimited: No specified limit of measurement readings.
				- COUNt: Determines the number of measurement readings.
				- DURation: Sets a time interval between the measurement readings.
				- SPAN: Defines start time and time span for the measurement readings."""
		response = self._core.io.query_str('LOG:MODE?')
		return Conversions.str_to_scalar_enum(response, enums.LoggingMode)

	def set_mode(self, logging_mode: enums.LoggingMode) -> None:
		"""SCPI: LOG:MODE \n
		Snippet: driver.log.set_mode(logging_mode = enums.LoggingMode.COUNt) \n
		Selects the data logging mode. \n
			:param logging_mode:
				- UNLimited: No specified limit of measurement readings.
				- COUNt: Determines the number of measurement readings.
				- DURation: Sets a time interval between the measurement readings.
				- SPAN: Defines start time and time span for the measurement readings."""
		param = Conversions.enum_scalar_to_str(logging_mode, enums.LoggingMode)
		self._core.io.write(f'LOG:MODE {param}')

	def get_fname(self) -> str:
		"""SCPI: LOG:FNAMe \n
		Snippet: value: str = driver.log.get_fname() \n
		Sets the file name and path for the storing the data recorded during data logging. The query returns the file name and
		path. You can query the information also when data logging is running. \n
			:return: logging_file_name: String with the directory and filename.
		"""
		response = self._core.io.query_str('LOG:FNAMe?')
		return trim_str_response(response)

	def set_fname(self, logging_file_name: str) -> None:
		"""SCPI: LOG:FNAMe \n
		Snippet: driver.log.set_fname(logging_file_name = '1') \n
		Sets the file name and path for the storing the data recorded during data logging. The query returns the file name and
		path. You can query the information also when data logging is running. \n
			:param logging_file_name: String with the directory and filename.
		"""
		param = Conversions.value_to_quoted_str(logging_file_name)
		self._core.io.write(f'LOG:FNAMe {param}')

	# noinspection PyTypeChecker
	class StimeStruct(StructBase):  # From WriteStructDefinition CmdPropertyTemplate.xml
		"""Structure for setting input parameters. Fields: \n
			- Year: int: Four-digit number, including the century and millennium information.
			- Month: int: No parameter help available
			- Day: int: No parameter help available
			- Hour: int: No parameter help available
			- Minute: int: No parameter help available
			- Second: int: No parameter help available"""
		__meta_args_list = [
			ArgStruct.scalar_int('Year'),
			ArgStruct.scalar_int('Month'),
			ArgStruct.scalar_int('Day'),
			ArgStruct.scalar_int('Hour'),
			ArgStruct.scalar_int('Minute'),
			ArgStruct.scalar_int('Second')]

		def __init__(self):
			StructBase.__init__(self, self)
			self.Year: int = None
			self.Month: int = None
			self.Day: int = None
			self.Hour: int = None
			self.Minute: int = None
			self.Second: int = None

	def get_stime(self) -> StimeStruct:
		"""SCPI: LOG:STIMe \n
		Snippet: value: StimeStruct = driver.log.get_stime() \n
		Sets the logging start time. \n
			:return: structure: for return value, see the help for StimeStruct structure arguments.
		"""
		return self._core.io.query_struct('LOG:STIMe?', self.__class__.StimeStruct())

	def set_stime(self, value: StimeStruct) -> None:
		"""SCPI: LOG:STIMe \n
		Snippet with structure: \n
		structure = driver.log.StimeStruct() \n
		structure.Year: int = 1 \n
		structure.Month: int = 1 \n
		structure.Day: int = 1 \n
		structure.Hour: int = 1 \n
		structure.Minute: int = 1 \n
		structure.Second: int = 1 \n
		driver.log.set_stime(value = structure) \n
		Sets the logging start time. \n
			:param value: see the help for StimeStruct structure arguments.
		"""
		self._core.io.write_struct('LOG:STIMe', value)
