from ...Internal.Core import Core
from ...Internal.CommandsGroup import CommandsGroup
from ...Internal import Conversions


# noinspection PyPep8Naming,PyAttributeOutsideInit,SpellCheckingInspection
class DataCls:
	"""Data commands group definition. 1 total commands, 0 Subgroups, 1 group commands"""

	def __init__(self, core: Core, parent):
		self._core = core
		self._cmd_group = CommandsGroup("data", core, parent)

	def get(self, file_path: str) -> bytes:
		"""SCPI: DATA:DATA \n
		Snippet: value: bytes = driver.data.data.get(file_path = '1') \n
		Queries the contents of a file, e.g. the data of a logging file. \n
			:param file_path: No help available
			:return: result: No help available"""
		param = Conversions.value_to_quoted_str(file_path)
		response = self._core.io.query_bin_block_ERROR(f'DATA:DATA? {param}')
		return response
