from ...Internal.Core import Core
from ...Internal.CommandsGroup import CommandsGroup
from ...Internal import Conversions


# noinspection PyPep8Naming,PyAttributeOutsideInit,SpellCheckingInspection
class PointsCls:
	"""Points commands group definition. 1 total commands, 0 Subgroups, 1 group commands"""

	def __init__(self, core: Core, parent):
		self._core = core
		self._cmd_group = CommandsGroup("points", core, parent)

	def get(self, file_path: str) -> int:
		"""SCPI: DATA:POINts \n
		Snippet: value: int = driver.data.points.get(file_path = r1) \n
		Queries the number of measurement readings saved in a file. \n
			:param file_path: No help available
			:return: count: No help available"""
		param = Conversions.value_to_str(file_path)
		response = self._core.io.query_str(f'DATA:POINts? {param}')
		return Conversions.str_to_int(response)
