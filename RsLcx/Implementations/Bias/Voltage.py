from ...Internal.Core import Core
from ...Internal.CommandsGroup import CommandsGroup
from ...Internal import Conversions


# noinspection PyPep8Naming,PyAttributeOutsideInit,SpellCheckingInspection
class VoltageCls:
	"""Voltage commands group definition. 1 total commands, 0 Subgroups, 1 group commands"""

	def __init__(self, core: Core, parent):
		self._core = core
		self._cmd_group = CommandsGroup("voltage", core, parent)

	def get_level(self) -> float:
		"""SCPI: BIAS:VOLTage[:LEVel] \n
		Snippet: value: float = driver.bias.voltage.get_level() \n
		Sets the internal DC bias voltage value. To activate the bias, use command method RsLcx.Bias.state. \n
			:return: bias_voltage_level: No help available
		"""
		response = self._core.io.query_str('BIAS:VOLTage:LEVel?')
		return Conversions.str_to_float(response)

	def set_level(self, bias_voltage_level: float) -> None:
		"""SCPI: BIAS:VOLTage[:LEVel] \n
		Snippet: driver.bias.voltage.set_level(bias_voltage_level = 1.0) \n
		Sets the internal DC bias voltage value. To activate the bias, use command method RsLcx.Bias.state. \n
			:param bias_voltage_level: No help available
		"""
		param = Conversions.decimal_value_to_str(bias_voltage_level)
		self._core.io.write(f'BIAS:VOLTage:LEVel {param}')
