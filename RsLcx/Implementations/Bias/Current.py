from ...Internal.Core import Core
from ...Internal.CommandsGroup import CommandsGroup
from ...Internal import Conversions


# noinspection PyPep8Naming,PyAttributeOutsideInit,SpellCheckingInspection
class CurrentCls:
	"""Current commands group definition. 1 total commands, 0 Subgroups, 1 group commands"""

	def __init__(self, core: Core, parent):
		self._core = core
		self._cmd_group = CommandsGroup("current", core, parent)

	def get_level(self) -> float:
		"""SCPI: BIAS:CURRent[:LEVel] \n
		Snippet: value: float = driver.bias.current.get_level() \n
		Sets the internal bias current value. To activate the bias, use command method RsLcx.Bias.state. \n
			:return: bias_current_level: No help available
		"""
		response = self._core.io.query_str('BIAS:CURRent:LEVel?')
		return Conversions.str_to_float(response)

	def set_level(self, bias_current_level: float) -> None:
		"""SCPI: BIAS:CURRent[:LEVel] \n
		Snippet: driver.bias.current.set_level(bias_current_level = 1.0) \n
		Sets the internal bias current value. To activate the bias, use command method RsLcx.Bias.state. \n
			:param bias_current_level: No help available
		"""
		param = Conversions.decimal_value_to_str(bias_current_level)
		self._core.io.write(f'BIAS:CURRent:LEVel {param}')
