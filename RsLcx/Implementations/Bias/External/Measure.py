from ....Internal.Core import Core
from ....Internal.CommandsGroup import CommandsGroup
from ....Internal import Conversions


# noinspection PyPep8Naming,PyAttributeOutsideInit,SpellCheckingInspection
class MeasureCls:
	"""Measure commands group definition. 1 total commands, 0 Subgroups, 1 group commands"""

	def __init__(self, core: Core, parent):
		self._core = core
		self._cmd_group = CommandsGroup("measure", core, parent)

	def get_voltage(self) -> float:
		"""SCPI: BIAS:EXTernal:MEASure:VOLTage \n
		Snippet: value: float = driver.bias.external.measure.get_voltage() \n
		Queries the value of the externally applied voltage. \n
			:return: voltage: No help available
		"""
		response = self._core.io.query_str('BIAS:EXTernal:MEASure:VOLTage?')
		return Conversions.str_to_float(response)
