from ....Internal.Core import Core
from ....Internal.CommandsGroup import CommandsGroup
from ....Internal import Conversions


# noinspection PyPep8Naming,PyAttributeOutsideInit,SpellCheckingInspection
class VoltageCls:
	"""Voltage commands group definition. 1 total commands, 0 Subgroups, 1 group commands"""

	def __init__(self, core: Core, parent):
		self._core = core
		self._cmd_group = CommandsGroup("voltage", core, parent)

	def get_state(self) -> bool:
		"""SCPI: BIAS:EXTernal[:VOLTage][:STATe] \n
		Snippet: value: bool = driver.bias.external.voltage.get_state() \n
		Activates the externally supplied bias voltage. \n
			:return: external_voltage_bias_state: No help available
		"""
		response = self._core.io.query_str('BIAS:EXTernal:VOLTage:STATe?')
		return Conversions.str_to_bool(response)

	def set_state(self, external_voltage_bias_state: bool) -> None:
		"""SCPI: BIAS:EXTernal[:VOLTage][:STATe] \n
		Snippet: driver.bias.external.voltage.set_state(external_voltage_bias_state = False) \n
		Activates the externally supplied bias voltage. \n
			:param external_voltage_bias_state: No help available
		"""
		param = Conversions.bool_to_str(external_voltage_bias_state)
		self._core.io.write(f'BIAS:EXTernal:VOLTage:STATe {param}')
