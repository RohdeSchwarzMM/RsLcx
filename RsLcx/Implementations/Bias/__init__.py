from ...Internal.Core import Core
from ...Internal.CommandsGroup import CommandsGroup
from ...Internal import Conversions


# noinspection PyPep8Naming,PyAttributeOutsideInit,SpellCheckingInspection
class BiasCls:
	"""Bias commands group definition. 5 total commands, 3 Subgroups, 1 group commands"""

	def __init__(self, core: Core, parent):
		self._core = core
		self._cmd_group = CommandsGroup("bias", core, parent)

	@property
	def current(self):
		"""current commands group. 0 Sub-classes, 1 commands."""
		if not hasattr(self, '_current'):
			from .Current import CurrentCls
			self._current = CurrentCls(self._core, self._cmd_group)
		return self._current

	@property
	def voltage(self):
		"""voltage commands group. 0 Sub-classes, 1 commands."""
		if not hasattr(self, '_voltage'):
			from .Voltage import VoltageCls
			self._voltage = VoltageCls(self._core, self._cmd_group)
		return self._voltage

	@property
	def external(self):
		"""external commands group. 2 Sub-classes, 0 commands."""
		if not hasattr(self, '_external'):
			from .External import ExternalCls
			self._external = ExternalCls(self._core, self._cmd_group)
		return self._external

	def get_state(self) -> bool:
		"""SCPI: BIAS:STATe \n
		Snippet: value: bool = driver.bias.get_state() \n
		Activates the internal DC bias. To set the corresponding bias voltage or current value, use commands method RsLcx.Bias.
		Current.level, or method RsLcx.Bias.Voltage.level. \n
			:return: bias_state: No help available
		"""
		response = self._core.io.query_str('BIAS:STATe?')
		return Conversions.str_to_bool(response)

	def set_state(self, bias_state: bool) -> None:
		"""SCPI: BIAS:STATe \n
		Snippet: driver.bias.set_state(bias_state = False) \n
		Activates the internal DC bias. To set the corresponding bias voltage or current value, use commands method RsLcx.Bias.
		Current.level, or method RsLcx.Bias.Voltage.level. \n
			:param bias_state: No help available
		"""
		param = Conversions.bool_to_str(bias_state)
		self._core.io.write(f'BIAS:STATe {param}')
