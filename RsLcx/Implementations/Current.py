from ..Internal.Core import Core
from ..Internal.CommandsGroup import CommandsGroup
from ..Internal import Conversions


# noinspection PyPep8Naming,PyAttributeOutsideInit,SpellCheckingInspection
class CurrentCls:
	"""Current commands group definition. 1 total commands, 0 Subgroups, 1 group commands"""

	def __init__(self, core: Core, parent):
		self._core = core
		self._cmd_group = CommandsGroup("current", core, parent)

	def get_level(self) -> float:
		"""SCPI: CURRent[:LEVel] \n
		Snippet: value: float = driver.current.get_level() \n
		Sets the test signal current in RMS (root mean square) . \n
			:return: current_level: No help available
		"""
		response = self._core.io.query_str('CURRent:LEVel?')
		return Conversions.str_to_float(response)

	def set_level(self, current_level: float) -> None:
		"""SCPI: CURRent[:LEVel] \n
		Snippet: driver.current.set_level(current_level = 1.0) \n
		Sets the test signal current in RMS (root mean square) . \n
			:param current_level: No help available
		"""
		param = Conversions.decimal_value_to_str(current_level)
		self._core.io.write(f'CURRent:LEVel {param}')
