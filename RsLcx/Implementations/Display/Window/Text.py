from ....Internal.Core import Core
from ....Internal.CommandsGroup import CommandsGroup
from ....Internal import Conversions


# noinspection PyPep8Naming,PyAttributeOutsideInit,SpellCheckingInspection
class TextCls:
	"""Text commands group definition. 2 total commands, 0 Subgroups, 2 group commands"""

	def __init__(self, core: Core, parent):
		self._core = core
		self._cmd_group = CommandsGroup("text", core, parent)

	def set_data(self, message: str) -> None:
		"""SCPI: DISPlay[:WINDow]:TEXT[:DATA] \n
		Snippet: driver.display.window.text.set_data(message = r1) \n
		Enables you to post a text message on the display. To close user defined message, use command method RsLcx.Display.Window.
		Text.clear. \n
			:param message: Text message for display.
		"""
		param = Conversions.value_to_str(message)
		self._core.io.write(f'DISPlay:WINDow:TEXT:DATA {param}')

	def clear(self) -> None:
		"""SCPI: DISPlay[:WINDow]:TEXT:CLEar \n
		Snippet: driver.display.window.text.clear() \n
		Closes a user defined text message on the display. To create an own message, use command method RsLcx.Display.Window.Text.
		data. \n
		"""
		self._core.io.write(f'DISPlay:WINDow:TEXT:CLEar')

	def clear_with_opc(self, opc_timeout_ms: int = -1) -> None:
		"""SCPI: DISPlay[:WINDow]:TEXT:CLEar \n
		Snippet: driver.display.window.text.clear_with_opc() \n
		Closes a user defined text message on the display. To create an own message, use command method RsLcx.Display.Window.Text.
		data. \n
		Same as clear, but waits for the operation to complete before continuing further. Use the RsLcx.utilities.opc_timeout_set() to set the timeout value. \n
			:param opc_timeout_ms: Maximum time to wait in milliseconds, valid only for this call."""
		self._core.io.write_with_opc(f'DISPlay:WINDow:TEXT:CLEar', opc_timeout_ms)
