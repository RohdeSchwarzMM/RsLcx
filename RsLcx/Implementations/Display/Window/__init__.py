from ....Internal.Core import Core
from ....Internal.CommandsGroup import CommandsGroup


# noinspection PyPep8Naming,PyAttributeOutsideInit,SpellCheckingInspection
class WindowCls:
	"""Window commands group definition. 2 total commands, 1 Subgroups, 0 group commands"""

	def __init__(self, core: Core, parent):
		self._core = core
		self._cmd_group = CommandsGroup("window", core, parent)

	@property
	def text(self):
		"""text commands group. 0 Sub-classes, 2 commands."""
		if not hasattr(self, '_text'):
			from .Text import TextCls
			self._text = TextCls(self._core, self._cmd_group)
		return self._text
