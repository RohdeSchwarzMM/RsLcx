from typing import List

from ..Internal.Core import Core
from ..Internal.CommandsGroup import CommandsGroup


# noinspection PyPep8Naming,PyAttributeOutsideInit,SpellCheckingInspection
class FetchCls:
	"""Fetch commands group definition. 2 total commands, 0 Subgroups, 2 group commands"""

	def __init__(self, core: Core, parent):
		self._core = core
		self._cmd_group = CommandsGroup("fetch", core, parent)

	def get_impedance(self) -> List[float]:
		"""SCPI: FETCh:IMPedance \n
		Snippet: value: List[float] = driver.fetch.get_impedance() \n
		Queries the most recent valid values of the measured impedance. If no valid measurement values are available, the reponse
		reports error code -230. \n
			:return: impedance: No help available
		"""
		response = self._core.io.query_bin_or_ascii_float_list('FETCh:IMPedance?')
		return response

	def get_value(self) -> List[float]:
		"""SCPI: FETCh \n
		Snippet: value: List[float] = driver.fetch.get_value() \n
		Queries the most recent valid values for measurement pair 2. If no valid measurement values are available, the reponse
		reports error code -230. \n
			:return: results: No help available
		"""
		response = self._core.io.query_bin_or_ascii_float_list('FETCh?')
		return response
