from typing import List

from ..Internal.Core import Core
from ..Internal.CommandsGroup import CommandsGroup


# noinspection PyPep8Naming,PyAttributeOutsideInit,SpellCheckingInspection
class ReadCls:
	"""Read commands group definition. 2 total commands, 0 Subgroups, 2 group commands"""

	def __init__(self, core: Core, parent):
		self._core = core
		self._cmd_group = CommandsGroup("read", core, parent)

	def get_impedance(self) -> List[float]:
		"""SCPI: READ:IMPedance \n
		Snippet: value: List[float] = driver.read.get_impedance() \n
		Queries the impedance measurement results. \n
			:return: impedance: No help available
		"""
		response = self._core.io.query_bin_or_ascii_float_list('READ:IMPedance?')
		return response

	def get_value(self) -> List[float]:
		"""SCPI: READ \n
		Snippet: value: List[float] = driver.read.get_value() \n
		Queries the measurement results for measurement pair 2. \n
			:return: results: No help available
		"""
		response = self._core.io.query_bin_or_ascii_float_list('READ?')
		return response
