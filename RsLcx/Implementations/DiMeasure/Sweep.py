from ...Internal.Core import Core
from ...Internal.CommandsGroup import CommandsGroup
from ...Internal import Conversions
from ... import enums


# noinspection PyPep8Naming,PyAttributeOutsideInit,SpellCheckingInspection
class SweepCls:
	"""Sweep commands group definition. 3 total commands, 0 Subgroups, 3 group commands"""

	def __init__(self, core: Core, parent):
		self._core = core
		self._cmd_group = CommandsGroup("sweep", core, parent)

	# noinspection PyTypeChecker
	def get_parameter(self) -> enums.SweepParameter:
		"""SCPI: DIMeasure:SWEep:PARameter \n
		Snippet: value: enums.SweepParameter = driver.diMeasure.sweep.get_parameter() \n
		Selects the measurement parameter that varies in defined steps within the sweep range (method RsLcx.DiMeasure.Sweep.
		minimum. \n
			:return: sweep_parameter:
				- VOLTage: Sweeps the test signal voltage.
				- FREQuency: Sweeps the test signal frequency.
				- VBIas: Sweeps the voltage bias.
				- IBIas: Sweeps the current bias."""
		response = self._core.io.query_str('DIMeasure:SWEep:PARameter?')
		return Conversions.str_to_scalar_enum(response, enums.SweepParameter)

	def set_parameter(self, sweep_parameter: enums.SweepParameter) -> None:
		"""SCPI: DIMeasure:SWEep:PARameter \n
		Snippet: driver.diMeasure.sweep.set_parameter(sweep_parameter = enums.SweepParameter.FREQuency) \n
		Selects the measurement parameter that varies in defined steps within the sweep range (method RsLcx.DiMeasure.Sweep.
		minimum. \n
			:param sweep_parameter:
				- VOLTage: Sweeps the test signal voltage.
				- FREQuency: Sweeps the test signal frequency.
				- VBIas: Sweeps the voltage bias.
				- IBIas: Sweeps the current bias."""
		param = Conversions.enum_scalar_to_str(sweep_parameter, enums.SweepParameter)
		self._core.io.write(f'DIMeasure:SWEep:PARameter {param}')

	def get_minimum(self) -> float:
		"""SCPI: DIMeasure:SWEep:MINimum \n
		Snippet: value: float = driver.diMeasure.sweep.get_minimum() \n
		Sets the start value for the selected sweep parameter. The value depends on the instrument model and installed options.
		The maximum value must be at least method RsLcx.DiMeasure.Sweep.maximum. \n
			:return: sweep_start_value: No help available
		"""
		response = self._core.io.query_str('DIMeasure:SWEep:MINimum?')
		return Conversions.str_to_float(response)

	def set_minimum(self, sweep_start_value: float) -> None:
		"""SCPI: DIMeasure:SWEep:MINimum \n
		Snippet: driver.diMeasure.sweep.set_minimum(sweep_start_value = 1.0) \n
		Sets the start value for the selected sweep parameter. The value depends on the instrument model and installed options.
		The maximum value must be at least method RsLcx.DiMeasure.Sweep.maximum. \n
			:param sweep_start_value: No help available
		"""
		param = Conversions.decimal_value_to_str(sweep_start_value)
		self._core.io.write(f'DIMeasure:SWEep:MINimum {param}')

	def get_maximum(self) -> float:
		"""SCPI: DIMeasure:SWEep:MAXimum \n
		Snippet: value: float = driver.diMeasure.sweep.get_maximum() \n
		Sets the stop value for the selected sweep parameter. The value must be at least method RsLcx.DiMeasure.Sweep.minimum.
		The maximum value depends on the instrument model and installed options. \n
			:return: sweep_stop_value: No help available
		"""
		response = self._core.io.query_str('DIMeasure:SWEep:MAXimum?')
		return Conversions.str_to_float(response)

	def set_maximum(self, sweep_stop_value: float) -> None:
		"""SCPI: DIMeasure:SWEep:MAXimum \n
		Snippet: driver.diMeasure.sweep.set_maximum(sweep_stop_value = 1.0) \n
		Sets the stop value for the selected sweep parameter. The value must be at least method RsLcx.DiMeasure.Sweep.minimum.
		The maximum value depends on the instrument model and installed options. \n
			:param sweep_stop_value: No help available
		"""
		param = Conversions.decimal_value_to_str(sweep_stop_value)
		self._core.io.write(f'DIMeasure:SWEep:MAXimum {param}')
