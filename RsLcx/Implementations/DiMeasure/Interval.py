from ...Internal.Core import Core
from ...Internal.CommandsGroup import CommandsGroup
from ...Internal import Conversions
from ... import enums


# noinspection PyPep8Naming,PyAttributeOutsideInit,SpellCheckingInspection
class IntervalCls:
	"""Interval commands group definition. 3 total commands, 0 Subgroups, 3 group commands"""

	def __init__(self, core: Core, parent):
		self._core = core
		self._cmd_group = CommandsGroup("interval", core, parent)

	# noinspection PyTypeChecker
	def get_type_py(self) -> enums.IntervalParameter:
		"""SCPI: DIMeasure:INTerval:TYPE \n
		Snippet: value: enums.IntervalParameter = driver.diMeasure.interval.get_type_py() \n
		Selects the mode to determine the measurement steps within the sweep range (method RsLcx.DiMeasure.Sweep.
		minimum to method RsLcx.DiMeasure.Sweep.maximum) . \n
			:return: interval_parameter:
				- STEPsize: Measures in defined step sizes within the sweep range, set with DIMeasure:INTerval:STEPsize.
				- POINts: Measures in increments calculated by the number of sweep points (DIMeasure:INTerval:POINts) within the sweep range."""
		response = self._core.io.query_str('DIMeasure:INTerval:TYPE?')
		return Conversions.str_to_scalar_enum(response, enums.IntervalParameter)

	def set_type_py(self, interval_parameter: enums.IntervalParameter) -> None:
		"""SCPI: DIMeasure:INTerval:TYPE \n
		Snippet: driver.diMeasure.interval.set_type_py(interval_parameter = enums.IntervalParameter.POINts) \n
		Selects the mode to determine the measurement steps within the sweep range (method RsLcx.DiMeasure.Sweep.
		minimum to method RsLcx.DiMeasure.Sweep.maximum) . \n
			:param interval_parameter:
				- STEPsize: Measures in defined step sizes within the sweep range, set with DIMeasure:INTerval:STEPsize.
				- POINts: Measures in increments calculated by the number of sweep points (DIMeasure:INTerval:POINts) within the sweep range."""
		param = Conversions.enum_scalar_to_str(interval_parameter, enums.IntervalParameter)
		self._core.io.write(f'DIMeasure:INTerval:TYPE {param}')

	def get_stepsize(self) -> float:
		"""SCPI: DIMeasure:INTerval:STEPsize \n
		Snippet: value: float = driver.diMeasure.interval.get_stepsize() \n
		Sets the step size within the measurement range for interval type method RsLcx.DiMeasure.Interval.typePy. \n
			:return: interval_stepsize: No help available
		"""
		response = self._core.io.query_str('DIMeasure:INTerval:STEPsize?')
		return Conversions.str_to_float(response)

	def set_stepsize(self, interval_stepsize: float) -> None:
		"""SCPI: DIMeasure:INTerval:STEPsize \n
		Snippet: driver.diMeasure.interval.set_stepsize(interval_stepsize = 1.0) \n
		Sets the step size within the measurement range for interval type method RsLcx.DiMeasure.Interval.typePy. \n
			:param interval_stepsize: No help available
		"""
		param = Conversions.decimal_value_to_str(interval_stepsize)
		self._core.io.write(f'DIMeasure:INTerval:STEPsize {param}')

	def get_points(self) -> float:
		"""SCPI: DIMeasure:INTerval:POINts \n
		Snippet: value: float = driver.diMeasure.interval.get_points() \n
		Sets the number of measurement points within the measurement range for interval type method RsLcx.DiMeasure.Interval.
		typePy. \n
			:return: interval_points: No help available
		"""
		response = self._core.io.query_str('DIMeasure:INTerval:POINts?')
		return Conversions.str_to_float(response)

	def set_points(self, interval_points: float) -> None:
		"""SCPI: DIMeasure:INTerval:POINts \n
		Snippet: driver.diMeasure.interval.set_points(interval_points = 1.0) \n
		Sets the number of measurement points within the measurement range for interval type method RsLcx.DiMeasure.Interval.
		typePy. \n
			:param interval_points: No help available
		"""
		param = Conversions.decimal_value_to_str(interval_points)
		self._core.io.write(f'DIMeasure:INTerval:POINts {param}')
