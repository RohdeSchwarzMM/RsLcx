from ...Internal.Core import Core
from ...Internal.CommandsGroup import CommandsGroup


# noinspection PyPep8Naming,PyAttributeOutsideInit,SpellCheckingInspection
class DiMeasureCls:
	"""DiMeasure commands group definition. 8 total commands, 3 Subgroups, 1 group commands"""

	def __init__(self, core: Core, parent):
		self._core = core
		self._cmd_group = CommandsGroup("diMeasure", core, parent)

	@property
	def execute(self):
		"""execute commands group. 0 Sub-classes, 1 commands."""
		if not hasattr(self, '_execute'):
			from .Execute import ExecuteCls
			self._execute = ExecuteCls(self._core, self._cmd_group)
		return self._execute

	@property
	def sweep(self):
		"""sweep commands group. 0 Sub-classes, 3 commands."""
		if not hasattr(self, '_sweep'):
			from .Sweep import SweepCls
			self._sweep = SweepCls(self._core, self._cmd_group)
		return self._sweep

	@property
	def interval(self):
		"""interval commands group. 0 Sub-classes, 3 commands."""
		if not hasattr(self, '_interval'):
			from .Interval import IntervalCls
			self._interval = IntervalCls(self._core, self._cmd_group)
		return self._interval

	def abort(self) -> None:
		"""SCPI: DIMeasure:ABORt \n
		Snippet: driver.diMeasure.abort() \n
		Stops a running dynamic impedance measurement. \n
		"""
		self._core.io.write(f'DIMeasure:ABORt')

	def abort_with_opc(self, opc_timeout_ms: int = -1) -> None:
		"""SCPI: DIMeasure:ABORt \n
		Snippet: driver.diMeasure.abort_with_opc() \n
		Stops a running dynamic impedance measurement. \n
		Same as abort, but waits for the operation to complete before continuing further. Use the RsLcx.utilities.opc_timeout_set() to set the timeout value. \n
			:param opc_timeout_ms: Maximum time to wait in milliseconds, valid only for this call."""
		self._core.io.write_with_opc(f'DIMeasure:ABORt', opc_timeout_ms)
