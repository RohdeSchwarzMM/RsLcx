from ..Internal.Core import Core
from ..Internal.CommandsGroup import CommandsGroup
from ..Internal import Conversions


# noinspection PyPep8Naming,PyAttributeOutsideInit,SpellCheckingInspection
class VoltageCls:
	"""Voltage commands group definition. 1 total commands, 0 Subgroups, 1 group commands"""

	def __init__(self, core: Core, parent):
		self._core = core
		self._cmd_group = CommandsGroup("voltage", core, parent)

	def get_level(self) -> float:
		"""SCPI: VOLTage[:LEVel] \n
		Snippet: value: float = driver.voltage.get_level() \n
		Sets the test signal voltage in RMS (root mean square) . \n
			:return: voltage_level:
				- numeric: Sets the value.
				- MIN | MINimum: Queries the lower limit of the signal level.
				- MAX | MAXimum: Queries the upper limit of the signal level.
				- DEF | DEFault: Queries the signal level the instrument sets by default."""
		response = self._core.io.query_str('VOLTage:LEVel?')
		return Conversions.str_to_float(response)

	def set_level(self, voltage_level: float) -> None:
		"""SCPI: VOLTage[:LEVel] \n
		Snippet: driver.voltage.set_level(voltage_level = 1.0) \n
		Sets the test signal voltage in RMS (root mean square) . \n
			:param voltage_level:
				- numeric: Sets the value.
				- MIN | MINimum: Queries the lower limit of the signal level.
				- MAX | MAXimum: Queries the upper limit of the signal level.
				- DEF | DEFault: Queries the signal level the instrument sets by default."""
		param = Conversions.decimal_value_to_str(voltage_level)
		self._core.io.write(f'VOLTage:LEVel {param}')
