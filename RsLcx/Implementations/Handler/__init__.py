from ...Internal.Core import Core
from ...Internal.CommandsGroup import CommandsGroup
from ...Internal import Conversions


# noinspection PyPep8Naming,PyAttributeOutsideInit,SpellCheckingInspection
class HandlerCls:
	"""Handler commands group definition. 5 total commands, 2 Subgroups, 1 group commands"""

	def __init__(self, core: Core, parent):
		self._core = core
		self._cmd_group = CommandsGroup("handler", core, parent)

	@property
	def config(self):
		"""config commands group. 0 Sub-classes, 1 commands."""
		if not hasattr(self, '_config'):
			from .Config import ConfigCls
			self._config = ConfigCls(self._core, self._cmd_group)
		return self._config

	@property
	def bin(self):
		"""bin commands group. 1 Sub-classes, 0 commands."""
		if not hasattr(self, '_bin'):
			from .Bin import BinCls
			self._bin = BinCls(self._core, self._cmd_group)
		return self._bin

	def get_state(self) -> bool:
		"""SCPI: HANDler[:STATe] \n
		Snippet: value: bool = driver.handler.get_state() \n
		Activates the binning measurement. \n
			:return: handler_state: No help available
		"""
		response = self._core.io.query_str('HANDler:STATe?')
		return Conversions.str_to_bool(response)

	def set_state(self, handler_state: bool) -> None:
		"""SCPI: HANDler[:STATe] \n
		Snippet: driver.handler.set_state(handler_state = False) \n
		Activates the binning measurement. \n
			:param handler_state: No help available
		"""
		param = Conversions.bool_to_str(handler_state)
		self._core.io.write(f'HANDler:STATe {param}')
