from ....Internal.Core import Core
from ....Internal.CommandsGroup import CommandsGroup


# noinspection PyPep8Naming,PyAttributeOutsideInit,SpellCheckingInspection
class BinCls:
	"""Bin commands group definition. 3 total commands, 1 Subgroups, 0 group commands"""

	def __init__(self, core: Core, parent):
		self._core = core
		self._cmd_group = CommandsGroup("bin", core, parent)

	@property
	def statistic(self):
		"""statistic commands group. 0 Sub-classes, 3 commands."""
		if not hasattr(self, '_statistic'):
			from .Statistic import StatisticCls
			self._statistic = StatisticCls(self._core, self._cmd_group)
		return self._statistic
