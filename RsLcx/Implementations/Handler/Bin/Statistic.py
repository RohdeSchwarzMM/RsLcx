from typing import List

from ....Internal.Core import Core
from ....Internal.CommandsGroup import CommandsGroup
from ....Internal import Conversions


# noinspection PyPep8Naming,PyAttributeOutsideInit,SpellCheckingInspection
class StatisticCls:
	"""Statistic commands group definition. 3 total commands, 0 Subgroups, 3 group commands"""

	def __init__(self, core: Core, parent):
		self._core = core
		self._cmd_group = CommandsGroup("statistic", core, parent)

	def get_count(self) -> int:
		"""SCPI: HANDler:BIN:STATistic:COUNt \n
		Snippet: value: int = driver.handler.bin.statistic.get_count() \n
		Queries the total number of samples measured since reset The query returns the sum of all counts. \n
			:return: count: No help available
		"""
		response = self._core.io.query_str('HANDler:BIN:STATistic:COUNt?')
		return Conversions.str_to_int(response)

	def reset(self) -> None:
		"""SCPI: HANDler:BIN:STATistic:RESet \n
		Snippet: driver.handler.bin.statistic.reset() \n
		Resets the evaluated binning measurement statistics. \n
		"""
		self._core.io.write(f'HANDler:BIN:STATistic:RESet')

	def reset_with_opc(self, opc_timeout_ms: int = -1) -> None:
		"""SCPI: HANDler:BIN:STATistic:RESet \n
		Snippet: driver.handler.bin.statistic.reset_with_opc() \n
		Resets the evaluated binning measurement statistics. \n
		Same as reset, but waits for the operation to complete before continuing further. Use the RsLcx.utilities.opc_timeout_set() to set the timeout value. \n
			:param opc_timeout_ms: Maximum time to wait in milliseconds, valid only for this call."""
		self._core.io.write_with_opc(f'HANDler:BIN:STATistic:RESet', opc_timeout_ms)

	def get_value(self) -> List[int]:
		"""SCPI: HANDler:BIN:STATistic \n
		Snippet: value: List[int] = driver.handler.bin.statistic.get_value() \n
		Queries the number of samples counted in the bins. The query returns 8 integer values. \n
			:return: statictics: No help available
		"""
		response = self._core.io.query_bin_or_ascii_int_list('HANDler:BIN:STATistic?')
		return response
