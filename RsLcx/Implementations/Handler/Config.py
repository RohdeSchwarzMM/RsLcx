from ...Internal.Core import Core
from ...Internal.CommandsGroup import CommandsGroup
from ...Internal import Conversions
from ...Internal.Utilities import trim_str_response


# noinspection PyPep8Naming,PyAttributeOutsideInit,SpellCheckingInspection
class ConfigCls:
	"""Config commands group definition. 1 total commands, 0 Subgroups, 1 group commands"""

	def __init__(self, core: Core, parent):
		self._core = core
		self._cmd_group = CommandsGroup("config", core, parent)

	def get_path(self) -> str:
		"""SCPI: HANDler:CONFig:PATH \n
		Snippet: value: str = driver.handler.config.get_path() \n
		Uploads the binning configuration file. \n
			:return: file_path: No help available
		"""
		response = self._core.io.query_str('HANDler:CONFig:PATH?')
		return trim_str_response(response)

	def set_path(self, file_path: str) -> None:
		"""SCPI: HANDler:CONFig:PATH \n
		Snippet: driver.handler.config.set_path(file_path = '1') \n
		Uploads the binning configuration file. \n
			:param file_path: No help available
		"""
		param = Conversions.value_to_quoted_str(file_path)
		self._core.io.write(f'HANDler:CONFig:PATH {param}')
