from ..Internal.Core import Core
from ..Internal.CommandsGroup import CommandsGroup
from ..Internal import Conversions


# noinspection PyPep8Naming,PyAttributeOutsideInit,SpellCheckingInspection
class FrequencyCls:
	"""Frequency commands group definition. 1 total commands, 0 Subgroups, 1 group commands"""

	def __init__(self, core: Core, parent):
		self._core = core
		self._cmd_group = CommandsGroup("frequency", core, parent)

	def get_cw(self) -> float:
		"""SCPI: FREQuency[:CW] \n
		Snippet: value: float = driver.frequency.get_cw() \n
		Sets the frequency of the test signal. \n
			:return: test_signal_frequency: No help available
		"""
		response = self._core.io.query_str('FREQuency:CW?')
		return Conversions.str_to_float(response)

	def set_cw(self, test_signal_frequency: float) -> None:
		"""SCPI: FREQuency[:CW] \n
		Snippet: driver.frequency.set_cw(test_signal_frequency = 1.0) \n
		Sets the frequency of the test signal. \n
			:param test_signal_frequency: No help available
		"""
		param = Conversions.decimal_value_to_str(test_signal_frequency)
		self._core.io.write(f'FREQuency:CW {param}')
