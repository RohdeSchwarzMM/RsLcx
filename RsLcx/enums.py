from enum import Enum


# noinspection SpellCheckingInspection
class HcopyFormat(Enum):
	"""2 Members, BMP ... PNG"""
	BMP = 0
	PNG = 1


# noinspection SpellCheckingInspection
class Impedance(Enum):
	"""4 Members, HIGH ... R100"""
	HIGH = 0
	LOW = 1
	R10 = 2
	R100 = 3


# noinspection SpellCheckingInspection
class ImpedanceType(Enum):
	"""24 Members, CPD ... ZTR"""
	CPD = 0
	CPG = 1
	CPQ = 2
	CPRP = 3
	CSD = 4
	CSQ = 5
	CSRS = 6
	GB = 7
	LPD = 8
	LPG = 9
	LPQ = 10
	LPRP = 11
	LSD = 12
	LSQ = 13
	LSRS = 14
	MTD = 15
	NTD = 16
	RDC = 17
	RPB = 18
	RX = 19
	YTD = 20
	YTR = 21
	ZTD = 22
	ZTR = 23


# noinspection SpellCheckingInspection
class IntervalParameter(Enum):
	"""2 Members, POINts ... STEPsize"""
	POINts = 0
	STEPsize = 1


# noinspection SpellCheckingInspection
class LoggingMode(Enum):
	"""4 Members, COUNt ... UNLimited"""
	COUNt = 0
	DURation = 1
	SPAN = 2
	UNLimited = 3


# noinspection SpellCheckingInspection
class MeasurementMode(Enum):
	"""2 Members, CONTinuous ... TRIGgered"""
	CONTinuous = 0
	TRIGgered = 1


# noinspection SpellCheckingInspection
class MeasurementTimeMode(Enum):
	"""4 Members, DEFault ... SHORt"""
	DEFault = 0
	LONG = 1
	MEDium = 2
	SHORt = 3


# noinspection SpellCheckingInspection
class MeasurementType(Enum):
	"""4 Members, C ... T"""
	C = 0
	L = 1
	R = 2
	T = 3


# noinspection SpellCheckingInspection
class MinOrMax(Enum):
	"""4 Members, MAX ... MINimum"""
	MAX = 0
	MAXimum = 1
	MIN = 2
	MINimum = 3


# noinspection SpellCheckingInspection
class SweepParameter(Enum):
	"""4 Members, FREQuency ... VOLTage"""
	FREQuency = 0
	IBIas = 1
	VBIas = 2
	VOLTage = 3


# noinspection SpellCheckingInspection
class TurnRatio(Enum):
	"""2 Members, N50 ... N500"""
	N50 = 0
	N500 = 1


# noinspection SpellCheckingInspection
class UsbClass(Enum):
	"""2 Members, CDC ... TMC"""
	CDC = 0
	TMC = 1
